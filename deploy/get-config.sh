#!/bin/bash

if [ -d "../deploy" ]; then
  # Go the root of the project
  cd ..
fi

source deploy/params.sh
source deploy/functions.sh

echo ''
echo ''
echo '----------------------------------------------------------'
pause 'Retrieve the config from production [Enter]'

# Export the config on the production server
ssh ${SERVER_USER}@${SERVER_DOMAIN} << EOF

# Go to the server path
cd "${SERVER_PATH}"

# Go to the project folder to be able to execute the Drush commands
cd web

# Clear the cache
${DRUSH_PATH} cex -y

EOF

# Retrieve the config and overwrite your local config
scp -r ${SERVER_USER}@${SERVER_DOMAIN}:${SERVER_PATH}/config .


echo ''
echo '----------------------------------------------------------'
pause 'Import the config from production into your local DB (CTRL + C to abort)[Enter]'

drush cim

echo ''
echo '----------------------------------------------------------'
echo 'Import finalized'
