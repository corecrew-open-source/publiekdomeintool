#!/bin/bash

if [ -d "../deploy" ]; then
  # Go the root of the project
  cd ..
fi

source deploy/params.sh
source deploy/functions.sh

DB_FILENAME=${PROJECT_NAME}_${CURRENT_DATETIME}.sql.gz

echo ''
echo ''
echo '----------------------------------------------------------'
pause 'Retrieve the db from production [Enter]'

ssh ${SERVER_USER}@${SERVER_DOMAIN} << EOF

# Go to the server path
cd "${SERVER_PATH}"

# Backup current db
if [ ! -d "db" ]; then
  mkdir db
fi

# Export DB
drush sql:dump --gzip  >> db/${DB_FILENAME}

EOF

# Retrieve the db
scp -r ${SERVER_USER}@${SERVER_DOMAIN}:${SERVER_PATH}/db/${DB_FILENAME} db/

echo ''
echo '----------------------------------------------------------'
echo 'Current database hostname : '
drush core-status --fields "DB hostname"
pause 'Import the DB from production into your local DB (CTRL + C to abort) [Enter]'

drush sql-drop
gunzip < db/${DB_FILENAME} | drush sql-cli

echo ''
echo '----------------------------------------------------------'
echo 'Import finalized'
