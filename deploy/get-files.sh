#!/bin/bash

if [ -d "../deploy" ]; then
  # Go the root of the project
  cd ..
fi

source deploy/params.sh
source deploy/functions.sh

echo ''
echo ''
echo '----------------------------------------------------------'
pause 'Retrieve the files folder from production [Enter]'

# Retrieve the config and overwrite your local config
scp -r ${SERVER_USER}@${SERVER_DOMAIN}:${SERVER_PATH}/web/sites/default/files web/sites/default/

echo ''
echo '----------------------------------------------------------'
echo 'Import finalized'
