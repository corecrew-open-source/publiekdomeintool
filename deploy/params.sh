#!/bin/bash

PROJECT_NAME="PLACEHOLDER"
SERVER_USER="PLACEHOLDER"
SERVER_DOMAIN="PLACEHOLDER"
SERVER_PATH="PLACEHOLDER"
IGNORE_PATH="deploy/paths.ignore"
INCLUDE_PATH="deploy/paths.include"
DRUSH_PATH='../vendor/drush/drush/drush'
CURRENT_DATETIME=`date +'%Y%m%d_%H%M%S'`
