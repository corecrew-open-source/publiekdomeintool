#!/usr/bin/env bash

cd /data/sites/web/publiekdomeintoolbe/www

/usr/local/bin/drush import athletes
/usr/local/bin/drush import politicians
/usr/local/bin/drush import artists
/usr/local/bin/drush import all
