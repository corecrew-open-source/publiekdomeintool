#!/bin/bash

PRODUCTION=no

if [ "$1" = "develop" ]
 then
  PATH_ROOT="/data/sites/web/publiekdomeintoolbe4530/subsites/publiekdomeintool.develop3.entityone.be";
fi

if [ "$1" = "release" ]
 then
  PATH_ROOT="/data/sites/web/publiekdomeintoolbe4530/subsites/publiekdomeintool.release3.entityone.be";
fi

if [ "$1" = "master" ]
 then
  PRODUCTION=yes
  # For zero-downtime deployments, your site name could be project name.
  # Define it here if needed.
  SITENAME="default"
  PATH_ROOT="/data/sites/web/publiekdomeintoolbe/www";

  # Path for the future live environment on E1 hosting
  #PATH_ROOT="/data/sites/web/publiekdomeintoolbe4530/subsites/publiekdomeintool.master3.entityone.be/current";
fi

PATH_WEB="$PATH_ROOT/web"
