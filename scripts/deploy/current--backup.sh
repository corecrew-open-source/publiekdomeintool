#!/bin/bash

# Backs up current live environment on zero-downtime deployments.
#
# USAGE:
# Run `./current--backup.sh <environment>
#
# Where <environment> is develop, release or master.
# This should be used for zero-downtime deploys in DeployHQ.
#
# Set up as "Before Release Link" SSH script.
# DeployHQ command:
# `cd %current_path%/scripts/deploy; ./current--backup.sh %environment%;`
#
# Make sure to check "Stop the deployment if the command fails?"

function _db_backup() {
  local site_name="$1"
  echo "Creating database backup in currently active release, just before new release gets activated."
  # Clear caches beforehand to save time and disk space.
  drush -l ${site_name} cr
  drush -l ${site_name} sql-dump --gzip --result-file=../zero_downtime_db_backup.$(date +%Y%m%d\-%H%M).sql
  echo "Backup available in previously active release folder."
}

# Load paths for this environment.
source config.sh $1

if [[ -v PATH_ROOT ]];
then
  cd $PATH_WEB || exit

  # Create a database backup in the currently active folder.
  # When using zero-downtime deployments, the current folder will be changed
  # immediately after this backup.
  # This means the DB backup will be stored in the archived folders only.

  _db_backup $SITENAME

else
  echo "ERROR: No environment found!"
  exit 1
fi
