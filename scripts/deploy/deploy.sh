#!/bin/bash

# Deploys database / config changes to the Drupal site.
#
# USAGE:
# Run `./deploy.sh <environment>
#
# Where <environment> is develop, release or master.
# This should be added to a DeployHQ "After changes" SSH script.
#
# DeployHQ command:
# `cd %path%/scripts/deploy; ./deploy.sh %environment%;`
# Or for zero-downtime deployments:
# `cd %current_path%/scripts/deploy; ./deploy.sh %environment%;`
#
# Make sure to check "Stop the deployment if the command fails?"

function _init_deployment() {
  local site_name="$1"
  local prod="$2"
  if [ "$prod" = yes ]; then
    # Set site in maintenance mode.
    drush -l ${site_name} sset system.maintenance_mode TRUE
  fi
}

function _composer_install() {
  local prod="$1"
  local ComposerVersion=$(composer --version)
  local Substring="Composer version 1"
  if [[ "$ComposerVersion" == *"$Substring"* ]]; then
    # Composer version 1 -> add --no-suggest option
    if [ "$prod" = yes ]; then
      composer install --no-dev --no-suggest --no-interaction
    else
      composer install --no-suggest --no-interaction
    fi
  else
    # Any other composer version
    if [ "$prod" = yes ]; then
      composer install --no-dev --no-interaction
    else
      composer install --no-interaction
    fi
  fi
}

function _drupal_execute_sync() {
  local site_name="$1"
  local prod="$2"

  # Cache rebuild.
  drush -l ${site_name} cr -y
  # Entity updates
  drush -l ${site_name} entup -y
  # Database updates
  drush -l ${site_name} updb -y
  # Cache rebuild.
  drush -l ${site_name} cr -y
  # Sync config
  drush -l ${site_name} cim -y
  # Sync config (do it twice to make sure)
  drush -l ${site_name} cim -y
  # Cache rebuild.
  drush -l ${site_name} cr -y
  # Node access rebuild (disabled)
  # drush -l ${site_name} php-eval 'node_access_rebuild()'

  if [ "$prod" = yes ]; then
    # Uninstall dev modules
    drush -l ${site_name} pm-uninstall devel -y
    drush -l ${site_name} pm-uninstall webprofiler -y
    # Disable maintenance mode.
    drush -l ${site_name} sset system.maintenance_mode FALSE
    # Cache rebuild.
    drush -l ${site_name} cr -y
  fi
}

# Load paths for this environment.
source config.sh $1

if [[ -v PATH_ROOT ]];
then
  # Initialize deployment.
  cd $PATH_WEB || exit
  _init_deployment $SITENAME $PRODUCTION

  # Chmod default directory to 777.
  chmod 777 "$PATH_WEB/sites/$SITENAME"

  # Composer install
  # This is used for non zero-downtime deploys. For zero-downtime deploys this
  # will do nothing, since composer will already have updated everything.
  cd $PATH_ROOT || exit
  _composer_install $PRODUCTION
  # Chmod default directory back to 555.
  chmod 555 "$PATH_WEB/sites/$SITENAME"


  # Sync all (sub)sites.
  cd $PATH_WEB || exit
  _drupal_execute_sync $SITENAME $PRODUCTION

  # EXAMPLE HOW TO EDIT robots.txt ON DEPLOY.
  #grep -qF -- "# Custom added paths" "robots.txt" || echo "# Custom added paths" >> "robots.txt"
  #grep -qF -- "Disallow: /calculate/*" "robots.txt" || echo "Disallow: /calculate/*" >> "robots.txt"
else
  echo "ERROR: No environment found!"
  exit 1
fi
