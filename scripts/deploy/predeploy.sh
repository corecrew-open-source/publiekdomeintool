#!/bin/bash

# Performs checks before deploy, to see if it is safe to do so.
#
# USAGE:
# Run `./predeploy.sh <environment>
#
# Where <environment> is develop, release or master.
# This should be added to a DeployHQ "Before changes" SSH script.
#
# DeployHQ command:
# `cd %path%/scripts/deploy; ./predeploy.sh %environment%;`
# Or for zero-downtime deployments:
# `cd %current_path%/scripts/deploy; ./predeploy.sh %environment%;`
#
# Make sure to check "Stop the deployment if the command fails?"

function _check_db_config_changes() {
  local site_name="$1"
  echo "Checking for DB config changes by running 'drush cst'"
  if [[ $(drush -l ${site_name} cst) ]]; then
      echo "WARNING: changes found - exiting deployment!"
      exit 1
  else
      echo "No changes found - continuing deployment"
  fi
}

# SCRIPT for avoiding overriding of config translation.
# PREREQUISITES:
# - add a `config/sync-temp` folder in your project root folder.
# - remove `config/sync/language` from project Git repo
# - Add ```config/sync/language``` to project .gitignore file
function _safeguard_language_config() {
  local site_name="$1"
  SYNC_FOLDER="../config/sync"
  SYNC_LANGUAGE_FOLDER="$SYNC_FOLDER/language"
  SYNC_TEMP_FOLDER="../config/sync-temp"
  SYNC_TEMP_LANGUAGE_FOLDER="$SYNC_TEMP_FOLDER/language"

  # Export the current database config to sync-temp folder.
  drush -l ${site_name} cex -y --destination=$SYNC_TEMP_FOLDER

  # Empty the current language folder.
  rm -rf $SYNC_LANGUAGE_FOLDER

  # Move the latest database config to the sync folder.
  # Leave other config untouched - only update language config.
  mv $SYNC_TEMP_LANGUAGE_FOLDER $SYNC_LANGUAGE_FOLDER

  # Empty the temp sync folder.
  EMPTY_FOLDER="$SYNC_TEMP_FOLDER/*"
  rm -rf $EMPTY_FOLDER
}

# Load paths for this environment.
source config.sh $1

if [[ -v PATH_ROOT ]];
then
  cd $PATH_WEB || exit

  # Safeguard language config in config/sync/language - so translations made
  # by client are not overridden on deploy. Read _safeguard_language_config
  # function documentation before use. Uncomment the following line to enable.

  # _safeguard_language_config $SITENAME

  # Check if there are changes between DB config and file system config.
  _check_db_config_changes $SITENAME

else
  echo "ERROR: No environment found!"
  exit 1
fi
