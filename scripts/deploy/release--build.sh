#!/bin/bash

# Builds the new release prior to being set active (zero-downtime deployments).
#
# USAGE:
# Run `./release--build.sh <environment>
#
# Where <environment> is develop, release or master.
# This should be used for zero-downtime deploys in DeployHQ.
#
# Set up as "Before Release Link" SSH script.
# DeployHQ command:
# `cd %release_path%/scripts/deploy; ./release--build.sh %environment%;`
#
# Make sure to check "Stop the deployment if the command fails?"

function _composer_install() {
  local prod="$1"
  if [ "$prod" = yes ]; then
    composer install --no-dev --no-suggest --no-interaction
  else
    composer install --no-suggest --no-interaction
  fi
}

# Load paths for this environment.
source config.sh $1

if [[ -v PATH_ROOT ]];
then
  # Composer install for zero-downtime deploys.
  # First, move from deploy folder to project root.
  cd ../..
  # Then, run composer install on release project (not yet active).
  _composer_install $PRODUCTION
else
  echo "ERROR: No environment found!"
  exit 1
fi
