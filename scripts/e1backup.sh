#!/bin/bash

die () {
    echo >&2 "$@"
    exit 1
}

[ $# -eq 1 ] || die "1 argument required, $# provided"

cd ../
NON_WEB_PATH=$PWD
WEB_FOLDER="web"
echo -e "Creating backups folder in root of project"
mkdir -p backups
echo -e "Changing directory to web/sites folder"
cd "$NON_WEB_PATH"/"$WEB_FOLDER"/sites/ || die "This path does not exist"
subsites=()
subsites+=($(ls -d */ | cut -f1 -d'/'))

if [ "$1" == "database" ]; then
  for i in "${subsites[@]}";
    do
      echo -e "Executing database backup on $i subsite";
      drush -l ${i} sql-dump --gzip --result-file="$NON_WEB_PATH"/backups/"${i}"_database_backup_"$(date +'%Y-%m-%d')".sql;
    done
  cd - || exit
elif [ "$1" == "files" ]; then
  for i in "${subsites[@]}";
    do
      echo -e "Executing files backup on $i subsite";

      cd "$NON_WEB_PATH"/"$WEB_FOLDER"/sites/"$i" || continue;
      if [[ -L files ]]; then
        tar -zcvf "$NON_WEB_PATH"/backups/"${i}"_files_backup_"$(date +'%Y-%m-%d')".tar.gz "$(readlink -f files)";
      else
        tar -zcvf "$NON_WEB_PATH"/backups/"${i}"_files_backup_"$(date +'%Y-%m-%d')".tar.gz files;
      fi
    done
  cd - || exit;
else
  echo -e "Wrong parameter was given, valid ones are: database, files"
fi
