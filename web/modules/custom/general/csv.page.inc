<?php

/**
 * @file
 * Contains csv.page.inc.
 *
 * Page callback for Csv entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Csv templates.
 *
 * Default template: csv.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_csv(array &$variables): void {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
