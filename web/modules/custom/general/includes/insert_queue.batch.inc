<?php

/**
 * @file
 * Batch operation logic for insert into queue.
 */

use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\general\Entity\Csv;

/**
 * Batch operation for batch 1: one at a time.
 *
 * This is the function that is called on each operation in batch 1.
 */
function insert_row_in_queue($data, $operation_details, &$context): void {
  $row = $data['row'];
  $step = $data['step'];
  $context['results']['generalData'] = $data['generalData'];

  // Get a queue (of the default type) called 'queue_example_queue'.
  // If the default queue class is SystemQueue this creates a queue that
  // stores its items in the database.
  $queue = \Drupal::service('queue')->get('csv_step_' . $step);
  // There is no harm in trying to recreate existing.
  $queue->createQueue();

  // Queue the string.
  $queue->createItem($data);

  // Message displayed under the progressbar.
  $context['message'] = t('Inserting',
    ['@id' => (int) $row[0] + 1, '@details' => $operation_details]
  );
}

/**
 * Batch 'finished' callback used by both batch 1 and batch 2.
 */
function insert_finished($success, $results, $operations) {
  if ($success) {
    // Add record to Db for webmaster to view in dashboard.
    $csv = Csv::create([
      'status' => 1,
      'field_file' => ['target_id' => $results['generalData']['fileId']],
      'field_did' => $results['generalData']['did'],
      'field_e_mail' => $results['generalData']['email'],
      'field_qid' => $results['generalData']['qid'],
      'field_step' => $results['generalData']['step'],
      'field_write' => $results['generalData']['write'],
      'field_total' => (int) $results['generalData']['numberOfRows'],
      'title' => $results['generalData']['did'],
      'field_processed' => 0,
      'field_processed_total' => 0,
      'field_completed' => 0,
      'uid' => 1,
    ]);
    $csv->save();

    // Redirect to confirm page.
    $aliasHelper = \Drupal::service('general.alias_helper');
    $alias = $aliasHelper->getAliasFromNid(7);
    return new RedirectResponse($alias);
  }
  else {
    $messenger = \Drupal::messenger();
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $messenger->addMessage(
      t('An error occurred while processing @operation with arguments : @args',
        [
          '@operation' => $error_operation[0],
          '@args' => print_r($error_operation[0], TRUE),
        ]
      )
      );
  }
}
