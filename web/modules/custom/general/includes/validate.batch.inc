<?php

/**
 * @file
 * Batch operation logic for validation.
 */

use Drupal\general\CsvErrorRowEnum;
use Drupal\general\CsvRowEnum;
use Drupal\file\Entity\File;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Batch operation for batch 1: one at a time.
 *
 * This is the function that is called on each operation in batch 1.
 */
function validate_row($data, $operation_details, &$context): void {
  $row = $data['row'];
  $rowNumber = $data['rowNumber'];
  $step = $data['step'];

  // Make instance of the custom validator.
  $validator = \Drupal::service('general.validator');

  // Write away general data.
  $context['results']['userData'] = $data['generalData'];
  $context['results']['step'] = $step;

  $numColumnsValid = TRUE;
  // Check the number of columns in the row.
  if (!$validator->validateNumberOfColumns($row, $step)) {
    if (!isset($context['results']['errors'][$rowNumber])) {
      $context['results']['errors'][$rowNumber]['row'] = $row;
    }
    $context['results']['errors'][$rowNumber]['general'] = [
      'error' => 'NUMBER_OF_COLUMNS',
      'column' => 0,
    ];
    $numColumnsValid = FALSE;
  }

  // Finish the batch if the are 1000 errors.
  if ((isset($context['results']['errors']) && count($context['results']['errors']) > 999) || (
      isset($context['results']['errors'][0]['general']) && $context['results']['errors'][0]['general']['error'] === 'NUMBER_OF_COLUMNS')
  ) {
    $context['finished'] = 1;
  }
  elseif ($numColumnsValid) {
    // Check column 'type auteur' when step is 2.
    if ($step === 2) {
      // Check the item has no error code or message (Foutcode / Foutmelding)
      // If so, fail validation and ask the user to remove these records.
      $columnNumberErrorCode = CsvErrorRowEnum::step1_error_code()->value;
      $columnNumberErrorMessage = CsvErrorRowEnum::step1_error_message()->value;
      if ((isset($row[$columnNumberErrorCode]) && isset($row[$columnNumberErrorMessage])) && ($row[$columnNumberErrorCode] !== '' || $row[$columnNumberErrorMessage] !== '')) {
        if (!isset($context['results']['errors'][$rowNumber])) {
          $context['results']['errors'][$rowNumber]['row'] = $row;
        }
        $context['results']['errors'][$rowNumber][$columnNumberErrorCode] = [
          'error' => 'SERVER_ERROR',
          'column' => $columnNumberErrorCode,
        ];
        $context['results']['errors'][$rowNumber][$columnNumberErrorMessage] = [
          'error' => 'SERVER_ERROR',
          'column' => $columnNumberErrorMessage,
        ];
      }

      $columnNumberTypeAuteur = CsvRowEnum::instance_of()->value;
      $allowedValues = [
        'Individu',
        'Organisatie',
      ];
      if ($row[$columnNumberTypeAuteur] === '' || !in_array($row[$columnNumberTypeAuteur], $allowedValues)) {
        if (!isset($context['results']['errors'][$rowNumber])) {
          $context['results']['errors'][$rowNumber]['row'] = $row;
        }
        $context['results']['errors'][$rowNumber][$columnNumberTypeAuteur] = [
          'error' => 'TYPE_AUTEUR',
          'column' => $columnNumberTypeAuteur,
        ];
      }
    }

    // Check the dates.
    $dateColumns = [
      CsvRowEnum::floruit()->value,
      CsvRowEnum::date_of_birth()->value,
      CsvRowEnum::date_of_death()->value,
    ];
    foreach ($dateColumns as $columnNumber) {
      if ($row[$columnNumber] !== '') {
        if (!$validator->validateDate($row[$columnNumber])) {
          if (!isset($context['results']['errors'][$rowNumber])) {
            $context['results']['errors'][$rowNumber]['row'] = $row;
          }
          $context['results']['errors'][$rowNumber][$columnNumber] = [
            'error' => 'DATE_FORMAT',
            'column' => $columnNumber,
          ];
        }
      }
    }

    // Check Qids.
    if ($row[CsvRowEnum::wikidata_id()->value] !== '') {
      if (!$validator->validateQid($row[CsvRowEnum::wikidata_id()->value])) {
        if (!isset($context['results']['errors'][$rowNumber])) {
          $context['results']['errors'][$rowNumber]['row'] = $row;
        }
        $context['results']['errors'][$rowNumber][CsvRowEnum::wikidata_id()->value] = [
          'error' => 'QID',
          'column' => CsvRowEnum::wikidata_id()->value,
        ];
      }
    }

    // Check VIAF, RKD, ULAN.
    $externalColumns = [8, 9, 10];
    foreach ($externalColumns as $columnNumber) {
      if ($step === 2) {
        $columnNumber = $columnNumber + 1;
      }
      if ($row[$columnNumber] !== '') {
        if (!$validator->validateNumber($row[$columnNumber])) {
          if (!isset($context['results']['errors'][$rowNumber])) {
            $context['results']['errors'][$rowNumber]['row'] = $row;
          }
          $context['results']['errors'][$rowNumber][$columnNumber] = [
            'error' => 'EXTERNAL',
            'column' => $columnNumber,
          ];
        }
      }
    }

    $context['results']['numberOfRows'] = $rowNumber;

    // Message displayed under the progressbar.
    $context['message'] = t('Validating',
      ['@id' => (int) $row[0] + 1, '@details' => $operation_details]
    );
  }
}

/**
 * Batch 'finished' callback used by both batch 1 and batch 2.
 */
function validate_finished($success, $results, $operations) {
  $messenger = \Drupal::messenger();
  if ($success) {
    $id = uniqid();
    $database = \Drupal::database();
    if (!isset($results['errors']) || count($results['errors']) === 0) {
      $query = $database->insert('valid')->fields([
        'did',
        'email',
        'created',
        'qid',
        'dataset_qid',
        'fileId',
        'filePath',
        'numberOfRows',
        'step',
        'write',
      ]);
      $query->values([
        'did' => $id,
        'qid' => $results['userData']['qid'],
        'dataset_qid' => $results['userData']['dataset_qid'],
        'email' => $results['userData']['email'],
        'created' => $results['userData']['time'],
        'fileId' => $results['userData']['fileId'],
        'filePath' => $results['userData']['filePath'],
        'numberOfRows' => ((int) $results['numberOfRows']) + 1,
        'step' => $results['step'],
        'write' => $results['userData']['write'],
      ]);
      $query->execute();

      // Trigger the second batch in the controller.
      return new RedirectResponse('/valid/' . $id);
    }
    else {
      // Remove the file.
      $file = File::load($results['userData']['fileId']);
      $file->delete();

      // Display the errors to the user.
      $query = $database->insert('errors')->fields([
        'did',
        'email',
        'created',
        'qid',
        'dataset_qid',
        'data',
      ]);
      $query->values([
        'did' => $id,
        'email' => $results['userData']['email'],
        'created' => $results['userData']['time'],
        'qid' => $results['userData']['qid'],
        'dataset_qid' => $results['userData']['dataset_qid'],
        'step' => $results['step'],
        'data' => json_encode(mb_convert_encoding($results, 'UTF-8', 'UTF-8')),
      ]);
      $query->execute();

      return new RedirectResponse('/errors/' . $id);
    }
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $messenger->addMessage(
      t('An error occurred while processing @operation with arguments : @args',
        [
          '@operation' => $error_operation[0],
          '@args' => print_r($error_operation[0], TRUE),
        ]
      )
    );
  }
  return NULL;
}
