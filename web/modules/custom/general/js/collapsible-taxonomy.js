(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.collapsibleTaxonomy = {
      attach: function (context, settings) {
        $('table#taxonomy tbody > tr').once('checkRows').each(function(){
            if ($(this).find('> td:first-child > .indentation').length > 0) {
                $(this).addClass('collapsible');
            } else {
                $(this).addClass('parent');
                $(this).find('> td:first-child').prepend('<span class="collapse"></span>');
            }
        });
        $('table#taxonomy tbody > tr').once('toggleCollapse').on('click', 'span.collapse', function(){
            $(this).closest('.parent').nextUntil('.parent').toggleClass('hidden');
            $(this).toggleClass('collapsed');
        });
      }
    };
  })(jQuery, Drupal, drupalSettings);
  