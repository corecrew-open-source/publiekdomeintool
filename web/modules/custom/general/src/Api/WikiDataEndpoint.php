<?php

namespace Drupal\general\Api;

/**
 * Class to define WikiData API endpoint.
 */
class WikiDataEndpoint {

  /**
   * The WikiData environment.
   *
   * @var string
   */
  protected string $env;

  /**
   * The endpoint URLs.
   *
   * @var array|string[]
   */
  protected array $urls = [
    'test' => 'https://test.wikidata.org',
    'live' => 'https://www.wikidata.org',
  ];

  /**
   * WikiData property information.
   *
   * @var array
   *
   * @todo convert row_idx values to CsvRowEnum value.
   */
  protected array $properties = [
    'test' => [
      'floruit' => [
        'property' => 'P96856',
        'row_idx' => 2,
      ],
      'date of birth' => [
        'property' => 'P18',
        'row_idx' => 5,
      ],
      'date of death' => [
        'property' => 'P25',
        'row_idx' => 6,
      ],
      'viaf' => [
        'property' => 'P765',
        'row_idx' => 9,
      ],
      'rkd' => [
        'property' => 'P97629',
        'row_idx' => 10,
      ],
      'ulan' => [
        'property' => 'P97630',
        'row_idx' => 11,
      ],
      'copyright status as a creator' => [
        'property' => 'P97631',
        'row_idx' => 13,
      ],
      'copyright representative' => [
        'property' => 'P97632',
        'row_idx' => 14,
      ],
      'instance of' => [
        'property' => 'P82',
        'row_idx' => NULL,
      ],
      'occupation' => [
        'property' => 'P204',
        'row_idx' => NULL,
      ],
      'has works in the collection' => [
        'property' => 'P97675',
        'row_idx' => NULL,
      ],
      'subclass of' => [
        'property' => 'P10208',
        'row_idx' => NULL,
      ],
      'reference url' => [
        'property' => 'P93',
        'row_idx' => NULL,
      ],
      'stated in' => [
        'property' => 'P149',
        'row_idx' => NULL,
      ],
      'quotation' => [
        'property' => 'P41231',
        'row_idx' => NULL,
      ],
      'retrieved' => [
        'property' => 'P388',
        'row_idx' => NULL,
      ],
      'collection' => [
        'property' => 'P94862',
        'row_idx' => NULL,
      ],
      'catalog' => [
        'property' => 'P73793',
        'row_idx' => NULL,
      ],
      'catalog code' => [
        'property' => 'P97963',
        'row_idx' => NULL,
      ],
    ],
    'live' => [
      'floruit' => [
        'property' => 'P1317',
        'row_idx' => 2,
      ],
      'date of birth' => [
        'property' => 'P569',
        'row_idx' => 5,
      ],
      'date of death' => [
        'property' => 'P570',
        'row_idx' => 6,
      ],
      'viaf' => [
        'property' => 'P214',
        'row_idx' => 9,
      ],
      'rkd' => [
        'property' => 'P650',
        'row_idx' => 10,
      ],
      'ulan' => [
        'property' => 'P245',
        'row_idx' => 11,
      ],
      'copyright status as a creator' => [
        'property' => 'P7763',
        'row_idx' => 13,
      ],
      'copyright representative' => [
        'property' => 'P6275',
        'row_idx' => 14,
      ],
      'instance of' => [
        'property' => 'P31',
        'row_idx' => NULL,
      ],
      'occupation' => [
        'property' => 'P106',
        'row_idx' => NULL,
      ],
      'has works in the collection' => [
        'property' => 'P6379',
        'row_idx' => NULL,
      ],
      'subclass of' => [
        'property' => 'P279',
        'row_idx' => NULL,
      ],
      'reference url' => [
        'property' => 'P854',
        'row_idx' => NULL,
      ],
      'stated in' => [
        'property' => 'P248',
        'row_idx' => NULL,
      ],
      'quotation' => [
        'property' => 'P1683',
        'row_idx' => NULL,
      ],
      'retrieved' => [
        'property' => 'P813',
        'row_idx' => NULL,
      ],
      'collection' => [
        'property' => 'P195',
        'row_idx' => NULL,
      ],
      'catalog' => [
        'property' => 'P972',
        'row_idx' => NULL,
      ],
      'catalog code' => [
        'property' => 'P217',
        'row_idx' => NULL,
      ],
    ],
  ];

  /**
   * Get the types of author.
   *
   * @var array
   */
  protected array $authorTypes = [
    'test' => [
      'Individu' => 'Q497',
      'Organisatie' => 'Q231167',
    ],
    'live' => [
      'Individu' => 'Q5',
      'Organisatie' => 'Q43229',
    ],
  ];

  /**
   * Creates a new WikiDataEndpoint.
   *
   * @param string|null $environment
   *   The environment.
   */
  public function __construct(string $environment = NULL) {
    if (!$environment) {
      $this->env = \Drupal::config('general.wikidata.settings')->get('wikidata_api_environment') ?? 'live';
    }
    else {
      $this->env = $environment;
    }
  }

  /**
   * Get the endpoint URL.
   *
   * @return string
   *   The endpoint URL.
   */
  public function getEndpointUrl(): string {
    return $this->urls[$this->env] . '/w/api.php';
  }

  /**
   * Get the endpoint base URL.
   *
   * @return string
   *   The endpoint base URL.
   */
  public function getEndpointBaseUrl(): string {
    return $this->urls[$this->env];
  }

  /**
   * Get a mapping between row ID and WikiData property.
   *
   * @return array
   *   List of properties, keyed by row ID.
   */
  public function getRowPropertyMapping(): array {
    $properties = $this->properties[$this->env];
    $mapping = [];
    foreach ($properties as $prop_values) {
      if ($prop_values['row_idx']) {
        $mapping[$prop_values['row_idx']] = $prop_values['property'];
      }
    }
    return $mapping;
  }

  /**
   * Get a mapping between WikiData property and row ID.
   *
   * @return array
   *   List of row IDs, keyed by property.
   */
  public function getPropertyRowMapping(): array {
    return array_flip($this->getRowPropertyMapping());
  }

  /**
   * Get the WikiData property for a given identifier.
   *
   * @param string $identifier
   *   The identifier for the property.
   *
   * @return string
   *   The WikiData property for the current environment.
   */
  public function getProperty(string $identifier): string {
    $properties = $this->properties[$this->env];
    return $properties[$identifier]['property'] ?? '';
  }

  /**
   * Get a mapping of author types.
   *
   * @return string[]
   *   List of author type WikiData QIDs, keyed by author type.
   */
  public function getAuthorTypes(): array {
    return $this->authorTypes[$this->env];
  }

  /**
   * Get the author type QID.
   *
   * @param string $identifier
   *   The identifier for the author type.
   *
   * @return string
   *   The WikiData QID.
   */
  public function getAuthorTypeId(string $identifier): string {
    return $this->getAuthorTypes()[$identifier] ?? '';
  }

  /**
   * Get date properties.
   *
   * @return array
   *   List of properties that are date-related.
   */
  public function getDateProperties(): array {
    $date_properties = [];
    $properties = $this->properties[$this->env];
    $date_properties[] = $properties['date of birth']['property'];
    $date_properties[] = $properties['date of death']['property'];
    $date_properties[] = $properties['floruit']['property'];
    return $date_properties;
  }

}
