<?php

namespace Drupal\general;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Batch service for general module.
 */
class BatchService {

  use StringTranslationTrait;

  /**
   * Batch process callback.
   *
   * @param array $data
   *   The data to import.
   * @param string $operation_details
   *   Details of the operation.
   * @param array $context
   *   Context for operations.
   *
   * @throws \Exception
   */
  public function import(array $data, string $operation_details, array &$context): void {
    $type = $data['type'];
    $rows = $data['data'];
    $database = \Drupal::database();
    foreach ($rows as $row) {
      $qid = $row['id']['value'];
      $qid = str_replace('http://www.wikidata.org/entity/', '', $qid);

      // Add extra detail to this query object: a condition, fields and a range.
      $query = $database->select('sparql', 'sparql');
      $query->condition('sparql.qid', $qid);
      $query->condition('sparql.type', $type);
      $query->fields('sparql', ['type']);
      $query->range(0, 1);
      $result = $query->execute();
      $data = $result->fetch();
      if ($data === FALSE) {
        $insertQuery = $database->insert('sparql')->fields(['qid', 'type']);
        $insertQuery->values([
          'qid' => $qid,
          'type' => $type,
        ]);
        $insertQuery->execute();
      }
    }

    // Optional message displayed under the progressbar.
    $context['message'] = $this->t('@details',
      ['@details' => $operation_details]
    );
  }

  /**
   * Batch Finished callback.
   *
   * @param bool $success
   *   Success of the operation.
   * @param array $results
   *   Array of results for post processing.
   * @param array $operations
   *   Array of operations.
   */
  public function importFinished(bool $success, array $results, array $operations): void {
    $messenger = \Drupal::messenger();
    if ($success) {
      // Here we could do something meaningful with the results.
      // We just display the number of nodes we processed...
      $messenger->addMessage($this->t('results processed.'));
    }
    else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      $messenger->addMessage(
        $this->t('An error occurred while processing @operation with arguments : @args',
          [
            '@operation' => $error_operation[0],
            '@args' => print_r($error_operation[0], TRUE),
          ]
        )
      );
    }
  }

}
