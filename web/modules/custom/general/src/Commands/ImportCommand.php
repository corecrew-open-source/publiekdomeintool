<?php

namespace Drupal\general\Commands;

use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
class ImportCommand extends DrushCommands {

  use StringTranslationTrait;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $drupalLogger;

  /**
   * ImportCommand constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   The logger channel factory.
   */
  public function __construct(LoggerChannelFactory $logger) {
    parent::__construct();
    $this->drupalLogger = $logger;
  }

  /**
   * Drush command for imports.
   *
   * @param string $type
   *   Argument provided to the drush command.
   *
   * @command import
   * @aliases import
   * @options arr An option that takes multiple values.
   * @options msg Whether or not an extra message should be displayed to the user.
   * @usage import $type
   */
  public function importCommand(string $type = 'athletes') {
    ini_set('memory_limit', -1);
    $allowedTypes = [
      'athletes',
      'politicians',
      'artists',
      'all',
    ];
    if (!in_array($type, $allowedTypes)) {
      var_dump('type is not allowed');
      exit;
    }

    // Log the start time.
    $this->output()->writeln("Start: " . (new \Datetime('now'))->format('d-m-Y H:i:s'));

    // 1. Log the start of the script.
    $this->drupalLogger->get('Import')->info('Import ' . $type . ' start.');

    // 2. Create the operations array for the batch.
    if ($type !== NULL) {
      $sparQl = \Drupal::service('general.sparqlhelper');
      switch ($type) {
        case 'athletes':
          $data = $sparQl->getAllAthleteCategories();
          break;

        case 'politicians':
          $data = $sparQl->getAllPoliticianCategories();
          break;

        case 'artists':
          $data = $sparQl->getAllArtistCategories();
          break;

        case 'all':
          $data = $sparQl->getAllCategories();
          break;
      }

      if ($type !== 'all') {
        $data = $data['results']['bindings'];
        $batch = $this->addToOperations($data, $type);
      }
      else {
        $newoperations = [];
        foreach ($data as $qid => $detailData) {
          $newoperations = array_merge($newoperations, $this->addToOperations($detailData['results']['bindings'], $qid)['operations']);
        }
        $batch = [
          'title' => $this->t('Updating  node(s)'),
          'operations' => $newoperations,
          'finished' => '\Drupal\general\BatchService::importFinished',
        ];
      }
    }

    // 4. Add batch operations as new batch sets.
    batch_set($batch);
    // 5. Process the batch sets.
    drush_backend_batch_process();
    // 6. Show some information.
    $this->logger()->notice("Batch operations end.");
    // 7. Log some information.
    $this->drupalLogger->get('Import')->info('Import ' . $type . ' end.');

    // Log the end time.
    $this->output()->writeln("End: " . (new \Datetime('now'))->format('d-m-Y H:i:s'));
  }

  /**
   *
   */
  private function groupRows($groupedRows, &$operations, $total, &$numOperations) {
    // Prepare the operation. Here we could do other operations on nodes.
    $operations[] = [
      '\Drupal\general\BatchService::import',
      [
        $groupedRows,
        t('Importing @item of @total', ['@item' => $numOperations + 1, '@total' => $total]),
      ],
    ];
  }

  /**
   *
   */
  private function addToOperations($data, $type) {
    $operations = [];
    $perPage = 100;
    $page = 0;
    $total = count($data);
    $numPages = (int) ceil($total / $perPage);
    $this->output()->writeln("Number of pages of " . $type . "s to import: " . $numPages);
    while ($page !== $numPages) {
      $this->output()->writeln("Getting date for page " . ($page + 1));
      $offset = 0;
      if ($page !== 0) {
        $offset = $page * $perPage;
      }
      $records = array_slice($data, $offset, $perPage);
      $groupedContent = [
        'type' => $type,
        'data' => $records,
      ];
      $this->groupRows($groupedContent, $operations, $numPages, $page);
      $page++;
    }

    // 3. Create the batch.
    return [
      'title' => t('Updating @num node(s)', ['@num' => $page]),
      'operations' => $operations,
      'finished' => '\Drupal\general\BatchService::importFinished',
    ];
  }

}
