<?php

namespace Drupal\general\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Url;
use Drupal\general\Service\Validator;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller for General module routes.
 */
class GeneralController extends ControllerBase {

  /**
   * GeneralController constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\general\Service\Validator $validator
   *   The validator service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   *   The module extension list service.
   */
  public function __construct(protected Connection $database, protected Validator $validator, protected ModuleExtensionList $moduleExtensionList) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('general.validator'),
      $container->get('extension.list.module')
    );
  }

  /**
   * Render errors display.
   */
  public function displayErrors($id): array {
    $query = $this->database->select('errors', 'e');

    // Add extra detail to this query object: a condition, fields and a range.
    $query->condition('e.did', $id);
    $query->fields('e', ['data', 'step']);
    $query->range(0, 1);
    $result = $query->execute();
    $data = $result->fetch();
    $data = json_decode($data->data, TRUE);

    $backlinkNode = 11;
    if ($data['step'] === 2) {
      $backlinkNode = 8;
    }
    $backlink = Url::fromRoute('entity.node.canonical', [
      'node' => $backlinkNode,
      'did' => $id,
    ]);

    return [
      '#theme' => 'errors',
      '#data' => $data['errors'],
      '#step' => $data['step'],
      '#back' => $backlink,
    ];
  }

  /**
   * Validates the CSV.
   *
   * @param string|int $id
   *   The CSV ID.
   */
  public function csvIsValid(string|int $id): ?RedirectResponse {
    $query = $this->database->select('valid', 'v');

    // Add extra detail to this query object: a condition, fields and a range.
    $query->condition('v.did', $id);
    $query->fields('v', [
      'qid',
      'dataset_qid',
      'email',
      'fileId',
      'filePath',
      'numberOfRows',
      'step',
      'write',
    ]);
    $query->range(0, 1);
    $result = $query->execute();
    $data = $result->fetch();
    $filePath = $data->filePath;

    $operations = [];
    $row = 0;
    $generalData = [
      'time' => strtotime('now'),
      'email' => $data->email,
      'qid' => $data->qid,
      'dataset_qid' => $data->dataset_qid,
      'did' => $id,
      'filePath' => $filePath,
      'fileId' => $data->fileId,
      'numberOfRows' => $data->numberOfRows,
      'write' => $data->write,
      'step' => $data->step,
    ];

    // Validate the rows.
    if (($handle = fopen($filePath, 'r')) !== FALSE) {
      $delimiter = $this->validator->getDelimiter($handle);
      while (($rowData = fgetcsv($handle, 0, $delimiter)) !== FALSE) {
        // Skip first line.
        if ($row !== 0) {
          $operations[] = [
            'insert_row_in_queue',
            [
              [
                'generalData' => $generalData,
                'rowNumber' => $row - 1,
                'row' => $rowData,
                'step' => $data->step,
              ],
              $this->t('Inserting', ['@operation' => $row - 1]),
            ],
          ];
        }
        $row++;
      }
      fclose($handle);
    }

    $batch = [
      'title' => $this->t('Inserting', ['@num' => count($operations)]),
      'operations' => $operations,
      'finished' => 'insert_finished',
      'file' => $this->moduleExtensionList->getPath('general') . '/includes/insert_queue.batch.inc',
    ];
    batch_set($batch);
    return batch_process('<front>');
  }

  /**
   * Import.
   */
  public function import() {
    $data = [
      0 =>
        [
          0 => 'painter (Q1028181)',
          1 => 'Q1028181',
        ],
      1 =>
        [
          0 => 'Q1028181',
          1 => 'schildering',
        ],
      2 =>
        [
          0 => 'Q1028181',
          1 => 'olieverfschildering',
        ],
      3 =>
        [
          0 => 'Q1028181',
          1 => 'schilderij',
        ],
      4 =>
        [
          0 => 'Q1028181',
          1 => 'schilderwerk',
        ],
      5 =>
        [
          0 => 'Q1028182',
          1 => 'gouache',
        ],
      6 =>
        [
          0 => 'Q1028183',
          1 => 'aquarel',
        ],
      7 =>
        [
          0 => 'Q1028184',
          1 => 'diptiek',
        ],
      8 =>
        [
          0 => 'Q1028185',
          1 => 'triptiek',
        ],
      9 =>
        [
          0 => 'Q1028186',
          1 => 'muurschildering',
        ],
      10 =>
        [
          0 => 'Q1028187',
          1 => 'olieverfschilderij',
        ],
      11 =>
        [
          0 => 'composer (Q36834)',
          1 => 'Q36834',
        ],
      12 =>
        [
          0 => 'Q36834',
          1 => 'muziekwerk',
        ],
      13 =>
        [
          0 => 'Q36835',
          1 => 'muziekstuk',
        ],
      14 =>
        [
          0 => 'Q36836',
          1 => 'lied',
        ],
      15 =>
        [
          0 => 'Q36837',
          1 => 'partituur',
        ],
      16 =>
        [
          0 => 'designer (Q5322166)',
          1 => 'Q5322166',
        ],
      17 =>
        [
          0 => 'Q5322166',
          1 => 'bouwwerk',
        ],
      18 =>
        [
          0 => 'Q5322167',
          1 => 'juweel',
        ],
      19 =>
        [
          0 => 'Q5322168',
          1 => 'bouwplan',
        ],
      20 =>
        [
          0 => 'Q5322169',
          1 => 'plan',
        ],
      21 =>
        [
          0 => 'Q5322170',
          1 => 'halssnoer',
        ],
      22 =>
        [
          0 => 'Q5322171',
          1 => 'hanger',
        ],
      23 =>
        [
          0 => 'Q5322172',
          1 => 'meubel',
        ],
      24 =>
        [
          0 => 'Q5322173',
          1 => 'medaille',
        ],
      25 =>
        [
          0 => 'Q5322174',
          1 => 'neusring',
        ],
      26 =>
        [
          0 => 'Q5322175',
          1 => 'oorring',
        ],
      27 =>
        [
          0 => 'Q5322176',
          1 => 'ring',
        ],
      28 =>
        [
          0 => 'Q5322177',
          1 => 'sierraad',
        ],
      29 =>
        [
          0 => 'visual artist (Q3391743)',
          1 => 'Q3391743',
        ],
      30 =>
        [
          0 => 'Q3391743',
          1 => 'beeldhouwwerk',
        ],
      31 =>
        [
          0 => 'Q3391744',
          1 => 'standbeeld',
        ],
      32 =>
        [
          0 => 'Q3391745',
          1 => 'tekening',
        ],
      33 =>
        [
          0 => 'Q3391746',
          1 => 'schets',
        ],
      34 =>
        [
          0 => 'Q3391747',
          1 => 'grafiek',
        ],
      35 =>
        [
          0 => 'Q3391748',
          1 => 'grafische kunst',
        ],
      36 =>
        [
          0 => 'Q3391749',
          1 => 'pentekening',
        ],
      37 =>
        [
          0 => 'Q3391750',
          1 => 'gravure',
        ],
      38 =>
        [
          0 => 'Q3391751',
          1 => 'prent',
        ],
      39 =>
        [
          0 => 'Q3391752',
          1 => 'cartoon',
        ],
      40 =>
        [
          0 => 'Q3391753',
          1 => 'litho',
        ],
      41 =>
        [
          0 => 'Q3391754',
          1 => 'lithografie',
        ],
      42 =>
        [
          0 => 'Q3391755',
          1 => 'keramiek',
        ],
      43 =>
        [
          0 => 'Q3391756',
          1 => 'houtsnede',
        ],
      44 =>
        [
          0 => 'Q3391757',
          1 => 'videowerk',
        ],
      45 =>
        [
          0 => 'Q3391758',
          1 => 'videokunst',
        ],
      46 =>
        [
          0 => 'Q3391759',
          1 => 'glaswerk',
        ],
      47 =>
        [
          0 => 'Q3391760',
          1 => 'glaskunst',
        ],
      48 =>
        [
          0 => 'Q3391761',
          1 => 'ets',
        ],
      49 =>
        [
          0 => 'Q3391762',
          1 => 'sculptuur',
        ],
      50 =>
        [
          0 => 'Q3391763',
          1 => 'mediakunst',
        ],
      51 =>
        [
          0 => 'Q3391764',
          1 => 'aquatint',
        ],
      52 =>
        [
          0 => 'Q3391765',
          1 => 'graffiti',
        ],
      53 =>
        [
          0 => 'Q3391766',
          1 => 'zeefdruk',
        ],
      54 =>
        [
          0 => 'Q3391767',
          1 => 'assemblage',
        ],
      55 =>
        [
          0 => 'Q3391768',
          1 => 'installatie',
        ],
      56 =>
        [
          0 => 'Q3391769',
          1 => 'beeld',
        ],
      57 =>
        [
          0 => 'Q3391770',
          1 => 'beeldje',
        ],
      58 =>
        [
          0 => 'Q3391771',
          1 => 'bas-relief',
        ],
      59 =>
        [
          0 => 'Q3391772',
          1 => 'buste',
        ],
      60 =>
        [
          0 => 'Q3391773',
          1 => 'collage',
        ],
      61 =>
        [
          0 => 'Q3391774',
          1 => 'heliogravure',
        ],
      62 =>
        [
          0 => 'Q3391775',
          1 => 'houtgravure',
        ],
      63 =>
        [
          0 => 'Q3391776',
          1 => 'houtskooltekening',
        ],
      64 =>
        [
          0 => 'Q3391777',
          1 => 'krijttekening',
        ],
      65 =>
        [
          0 => 'Q3391778',
          1 => 'linoleumsnede',
        ],
      66 =>
        [
          0 => 'Q3391779',
          1 => 'mezzotint',
        ],
      67 =>
        [
          0 => 'Q3391780',
          1 => 'mobile',
        ],
      68 =>
        [
          0 => 'Q3391781',
          1 => 'monoprint',
        ],
      69 =>
        [
          0 => 'Q3391782',
          1 => 'monotypie',
        ],
      70 =>
        [
          0 => 'Q3391783',
          1 => 'openbaar beeldhouwwerk',
        ],
      71 =>
        [
          0 => 'Q3391784',
          1 => 'reliëf',
        ],
      72 =>
        [
          0 => 'Q3391785',
          1 => 'video-installatie',
        ],
      73 =>
        [
          0 => 'Q3391786',
          1 => 'wandsculptuur',
        ],
      74 =>
        [
          0 => 'photographer (Q33231)',
          1 => 'Q33231',
        ],
      75 =>
        [
          0 => 'Q33231',
          1 => 'foto',
        ],
      76 =>
        [
          0 => 'Q33232',
          1 => 'negatief',
        ],
      77 =>
        [
          0 => 'Q33233',
          1 => 'kleurenfoto',
        ],
      78 =>
        [
          0 => 'Q33234',
          1 => 'zwart-witfoto',
        ],
      79 =>
        [
          0 => 'Q33235',
          1 => 'dia',
        ],
      80 =>
        [
          0 => 'Q33236',
          1 => 'diapositief',
        ],
      81 =>
        [
          0 => 'Q33237',
          1 => 'fotomontage',
        ],
      82 =>
        [
          0 => 'Q33238',
          1 => 'fotoboek',
        ],
      83 =>
        [
          0 => 'Q33239',
          1 => 'zwart-wit foto',
        ],
      84 =>
        [
          0 => 'author (Q482980)',
          1 => 'Q482980',
        ],
      85 =>
        [
          0 => 'Q482980',
          1 => 'roman',
        ],
      86 =>
        [
          0 => 'Q482981',
          1 => 'fictie',
        ],
      87 =>
        [
          0 => 'Q482982',
          1 => 'non-fictie',
        ],
      88 =>
        [
          0 => 'Q482983',
          1 => 'toneelstuk',
        ],
      89 =>
        [
          0 => 'Q482984',
          1 => 'theaterstuk',
        ],
      90 =>
        [
          0 => 'Q482985',
          1 => 'eenakter',
        ],
      91 =>
        [
          0 => 'Q482986',
          1 => 'monoloog',
        ],
      92 =>
        [
          0 => 'Q482987',
          1 => 'kinderboek',
        ],
      93 =>
        [
          0 => 'Q482988',
          1 => 'jeugdboek',
        ],
      94 =>
        [
          0 => 'Q482989',
          1 => 'kinderliteratuur',
        ],
      95 =>
        [
          0 => 'Q482990',
          1 => 'jongerenliteratuur',
        ],
      96 =>
        [
          0 => 'Q482991',
          1 => 'jeugdliteratuur',
        ],
      97 =>
        [
          0 => 'Q482992',
          1 => 'sprookje',
        ],
      98 =>
        [
          0 => 'Q482993',
          1 => 'liedtekst',
        ],
      99 =>
        [
          0 => 'Q482994',
          1 => 'songtekst',
        ],
      100 =>
        [
          0 => 'Q482995',
          1 => 'science fiction',
        ],
      101 =>
        [
          0 => 'Q482996',
          1 => 'manga',
        ],
      102 =>
        [
          0 => 'Q482997',
          1 => 'biografie',
        ],
      103 =>
        [
          0 => 'Q482998',
          1 => 'gedicht',
        ],
      104 =>
        [
          0 => 'Q482999',
          1 => 'proza',
        ],
      105 =>
        [
          0 => 'Q483000',
          1 => 'poëzie',
        ],
      106 =>
        [
          0 => 'Q483001',
          1 => 'libretto',
        ],
      107 =>
        [
          0 => 'Q483002',
          1 => 'kortverhaal',
        ],
      108 =>
        [
          0 => 'Q483003',
          1 => 'boek',
        ],
      109 =>
        [
          0 => 'Q483004',
          1 => 'detective',
        ],
      110 =>
        [
          0 => 'artisan (Q1294787)',
          1 => 'Q1294787',
        ],
      111 =>
        [
          0 => 'Q1294787',
          1 => 'kantwerk',
        ],
      112 =>
        [
          0 => 'Q1294788',
          1 => 'schotel',
        ],
      113 =>
        [
          0 => 'Q1294789',
          1 => 'tapijt',
        ],
      114 =>
        [
          0 => 'Q1294790',
          1 => 'textiel',
        ],
      115 =>
        [
          0 => 'Q1294791',
          1 => 'trofee',
        ],
      116 =>
        [
          0 => 'Q1294792',
          1 => 'vaandel',
        ],
      117 =>
        [
          0 => 'Q1294793',
          1 => 'vaas',
        ],
      118 =>
        [
          0 => 'Q1294794',
          1 => 'broderie',
        ],
      119 =>
        [
          0 => 'Q1294795',
          1 => 'wandtapijt',
        ],
      120 =>
        [
          0 => 'Q1294796',
          1 => 'borduurwerk',
        ],
      121 =>
        [
          0 => 'Ongecategoriseerd',
          1 => 'NONE',
        ],
      122 =>
        [
          0 => 'NONE',
          1 => 'manuscript (calligrapher Q3303330, author Q482980) ?',
        ],
      123 =>
        [
          0 => 'NONE',
          1 => 'strip',
        ],
      124 =>
        [
          0 => 'NONE',
          1 => 'stripverhaal',
        ],
      125 =>
        [
          0 => 'NONE',
          1 => 'stripalbum',
        ],
      126 =>
        [
          0 => 'NONE',
          1 => 'prentbriefkaart',
        ],
      127 =>
        [
          0 => 'NONE',
          1 => 'affiche',
        ],
      128 =>
        [
          0 => 'NONE',
          1 => 'film',
        ],
      129 =>
        [
          0 => 'NONE',
          1 => 'animatiefilm',
        ],
      130 =>
        [
          0 => 'NONE',
          1 => 'audiovisuele installatie',
        ],
      131 =>
        [
          0 => 'NONE',
          1 => 'digitale print',
        ],
      132 =>
        [
          0 => 'NONE',
          1 => 'print',
        ],
      133 =>
        [
          0 => 'NONE',
          1 => 'gemengde media',
        ],
      134 =>
        [
          0 => 'NONE',
          1 => 'mixed media',
        ],
      135 =>
        [
          0 => 'NONE',
          1 => 'laserprint',
        ],
      136 =>
        [
          0 => 'NONE',
          1 => 'lastechniek',
        ],
      137 =>
        [
          0 => 'NONE',
          1 => 'maquette',
        ],
      138 =>
        [
          0 => 'NONE',
          1 => 'offsetdruk',
        ],
      139 =>
        [
          0 => 'NONE',
          1 => 'pop',
        ],
      140 =>
        [
          0 => 'NONE',
          1 => 'postkaart',
        ],
      141 =>
        [
          0 => 'NONE',
          1 => 'toegepaste kunst',
        ],
    ];

    $parent = NULL;
    foreach ($data as $data_item) {
      if (!str_starts_with($data_item[0], 'Q') && $data_item[0] !== 'NONE') {
        $createData = [
          'vid' => 'categories',
          'name' => $data_item[0],
          'field_qid' => $data_item[1],
        ];
        $term = Term::create($createData);
        $term->enforceIsNew();
        // $term->parent = $parent_tid_none;
        $term->save();
        $parent = $term->id();
      }
      else {
        if (isset($parent)) {
          $createData = [
            'vid' => 'categories',
            'name' => $data_item[1],
          ];
          $term = Term::create($createData);
          $term->enforceIsNew();
          $term->parent = $parent;
          $term->save();
        }
      }
    }
  }

}
