<?php

namespace Drupal\general;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the CSV entity.
 *
 * @see \Drupal\general\Entity\Csv.
 */
class CsvAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {
    /** @var \Drupal\general\Entity\CsvInterface $entity */

    switch ($operation) {

      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished csv entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published csv entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit csv entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete csv entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResultInterface {
    return AccessResult::allowedIfHasPermission($account, 'add csv entities');
  }

}
