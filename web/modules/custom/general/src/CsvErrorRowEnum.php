<?php

namespace Drupal\general;

use Spatie\Enum\Enum;

/**
 * Enum for CSV error rows.
 *
 * @method static self step1_error_code()
 * @method static self step2_error_code()
 * @method static self step1_error_message()
 * @method static self step2_error_message()
 */
class CsvErrorRowEnum extends Enum {

  /**
   * {@inheritdoc}
   */
  protected static function values(): array {
    return [
      'step1_error_code' => 14,
      'step2_error_code' => 16,
      'step1_error_message' => 15,
      'step2_error_message' => 17,
    ];
  }

}
