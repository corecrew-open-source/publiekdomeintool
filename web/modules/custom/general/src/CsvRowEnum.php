<?php

namespace Drupal\general;

use Spatie\Enum\Enum;

/**
 * Enum for CSV rows.
 *
 * @method static self identifier()
 * @method static self object_name()
 * @method static self floruit()
 * @method static self location()
 * @method static self creator()
 * @method static self date_of_birth()
 * @method static self date_of_death()
 * @method static self wikidata_id()
 * @method static self wikidata_link()
 * @method static self viaf()
 * @method static self rkd()
 * @method static self ulan()
 * @method static self instance_of()
 * @method static self copyright_status()
 * @method static self public_year()
 * @method static self copyright_representative()
 */
class CsvRowEnum extends Enum {

  /**
   * {@inheritdoc}
   */
  protected static function values(): array {
    return [
      'identifier' => 0,
      'object_name' => 1,
      'floruit' => 2,
      'location' => 3,
      'creator' => 4,
      'date_of_birth' => 5,
      'date_of_death' => 6,
      'wikidata_id' => 7,
      'wikidata_link' => 8,
      'viaf' => 9,
      'rkd' => 10,
      'ulan' => 11,
      'instance_of' => 12,
      'copyright_status' => 13,
      'public_year' => 14,
      'copyright_representative' => 15,
    ];
  }

}
