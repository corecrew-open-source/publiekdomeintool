<?php

namespace Drupal\general\Data;

/**
 * Contains general data for batch processing.
 */
class BatchGeneralData {

  /**
   * The upload time.
   *
   * @var int|null
   */
  protected ?int $time = 0;

  /**
   * The email address to notify about progress.
   *
   * @var string|null
   */
  protected ?string $email = NULL;

  /**
   * The Wikidata QID of the organisation.
   *
   * @var string|null
   */
  protected ?string $organisationQid = NULL;

  /**
   * The Wikidata QID of the dataset.
   *
   * @var string|null
   */
  protected ?string $datasetQid = NULL;

  /**
   * The uploaded file path.
   *
   * @var string|null
   */
  protected ?string $filePath = NULL;

  /**
   * The uploaded file ID.
   *
   * @var string|null
   */
  protected ?string $fileId = NULL;

  /**
   * Whether to write to Wikidata.
   *
   * @var bool
   */
  protected bool $writeToWikidata = FALSE;

  /**
   * The CSV ID.
   *
   * @var string|null
   */
  protected ?string $csvId = NULL;

  /**
   * The number of rows.
   *
   * @var int|null
   */
  protected ?int $numberOfRows = NULL;

  /**
   * The current step.
   *
   * @var int|null
   */
  protected ?int $step = NULL;

  /**
   * Constructs a BatchGeneralData object.
   *
   * @param array $general_data
   *   The general data configured for batch processing.
   */
  public function __construct(array $general_data) {
    if (isset($general_data['time'])) {
      $this->time = $general_data['time'];
    }
    if (isset($general_data['email'])) {
      $this->email = $general_data['email'];
    }
    if (isset($general_data['qid'])) {
      $this->organisationQid = (string) $general_data['qid'];
    }
    if (isset($general_data['dataset_qid'])) {
      $this->datasetQid = (string) $general_data['dataset_qid'];
    }
    if (isset($general_data['filePath'])) {
      $this->filePath = (string) $general_data['filePath'];
    }

    if (isset($general_data['fileId'])) {
      $this->fileId = (string) $general_data['fileId'];
    }
    if (isset($general_data['write'])) {
      $this->writeToWikidata = (bool) $general_data['write'];
    }
    if (isset($general_data['did'])) {
      $this->csvId = (string) $general_data['did'];
    }
    if (isset($general_data['numberOfRows'])) {
      $this->numberOfRows = (int) $general_data['numberOfRows'];
    }
    if (isset($general_data['step'])) {
      $this->step = (int) $general_data['step'];
    }
  }

  /**
   * Get upload time.
   *
   * @return int|null
   *   The upload time.
   */
  public function getTime(): ?int {
    return $this->time;
  }


  /**
   * Get the notification email address.
   *
   * @return string|null
   *   The notification email address.
   */
  public function getEmail(): ?string {
    return $this->email;
  }

  /**
   * Get the organisation Wikidata QID.
   *
   * @return string|null
   *   The organisation Wikidata QID.
   */
  public function getOrganisationQid(): ?string {
    return $this->organisationQid;
  }

  /**
   * Get the dataset Wikidata QID.
   *
   * @return string|null
   *   The dataset Wikidata QID.
   */
  public function getDatasetQid(): ?string {
    return $this->datasetQid;
  }

  /**
   * Get the file upload path.
   *
   * @return string|null
   *   The file upload path.
   */
  public function getFilePath(): ?string {
    return $this->filePath;
  }

  /**
   * Get the uploaded file ID.
   *
   * @return string|null
   *   The uploaded file ID.
   */
  public function getFileId(): ?string {
    return $this->fileId;
  }

  /**
   * Whether to write to Wikidata.
   *
   * @return bool
   *   Whether to write to Wikidata.
   */
  public function writeToWikidata(): bool {
    return $this->writeToWikidata;
  }

  /**
   * Change flag to write to Wikidata.
   *
   * @param bool $writeToWikidata
   *   Whether to write to Wikidata.
   */
  public function setWriteToWikidata(bool $writeToWikidata): void {
    $this->writeToWikidata = $writeToWikidata;
  }

  /**
   * Get the CSV ID.
   *
   * @return string|null
   *   The CSV ID.
   */
  public function getCsvId(): ?string {
    return $this->csvId;
  }

  /**
   * Get the number of rows.
   *
   * @return int|null
   *   The number of rows.
   */
  public function getNumberOfRows(): ?int {
    return $this->numberOfRows;
  }

  /**
   * Get the step.
   *
   * @return int|null
   *   The current step.
   */
  public function getStep(): ?int {
    return $this->step;
  }

}
