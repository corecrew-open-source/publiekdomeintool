<?php

namespace Drupal\general\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Csv entities.
 */
interface CsvInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Gets the Csv name.
   *
   * @return string
   *   Name of the Csv.
   */
  public function getName();

  /**
   * Sets the Csv name.
   *
   * @param string $name
   *   The Csv name.
   *
   * @return \Drupal\general\Entity\CsvInterface
   *   The called Csv entity.
   */
  public function setName($name);

  /**
   * Gets the Csv creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Csv.
   */
  public function getCreatedTime();

  /**
   * Sets the Csv creation timestamp.
   *
   * @param int $timestamp
   *   The Csv creation timestamp.
   *
   * @return \Drupal\general\Entity\CsvInterface
   *   The called Csv entity.
   */
  public function setCreatedTime($timestamp);

}
