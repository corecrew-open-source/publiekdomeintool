<?php

namespace Drupal\general\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Wikidata update entities.
 */
interface WikidataUpdateInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Gets the Wikidata update name.
   *
   * @return string
   *   Name of the Wikidata update.
   */
  public function getName();

  /**
   * Sets the Wikidata update name.
   *
   * @param string $name
   *   The Wikidata update name.
   *
   * @return \Drupal\general\Entity\WikidataUpdateInterface
   *   The called Wikidata update entity.
   */
  public function setName($name);

  /**
   * Gets the Wikidata update creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Wikidata update.
   */
  public function getCreatedTime();

  /**
   * Sets the Wikidata update creation timestamp.
   *
   * @param int $timestamp
   *   The Wikidata update creation timestamp.
   *
   * @return \Drupal\general\Entity\WikidataUpdateInterface
   *   The called Wikidata update entity.
   */
  public function setCreatedTime($timestamp);

}
