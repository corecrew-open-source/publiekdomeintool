<?php

namespace Drupal\general\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Http\RequestStack;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\general\Service\Validator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * CSV Upload form - step 1.
 */
class UploadCsvForm extends FormBase {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected ?Request $currentRequest;

  /**
   * The file entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $fileStorage;

  /**
   * Constructor for UploadCsvForm.
   *
   * @param \Drupal\Core\File\FileSystem $fileSystem
   *   The file system service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Session\AccountProxyInterface $user
   *   The currently logged in user.
   * @param \Drupal\general\Service\Validator $validator
   *   The validator service.
   * @param \Drupal\Core\Http\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   *   The module extension list service.
   */
  public function __construct(protected FileSystem $fileSystem, protected Connection $database, protected AccountProxyInterface $user, protected Validator $validator, RequestStack $request_stack, EntityTypeManagerInterface $entity_type_manager, protected ModuleExtensionList $moduleExtensionList) {
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->fileStorage = $entity_type_manager->getStorage('file');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $form = new static(
      $container->get('file_system'),
      $container->get('database'),
      $container->get('current_user'),
      $container->get('general.validator'),
      $container->get('request_stack'),
      $container->get('entity_type.manager'),
      $container->get('extension.list.module')
    );
    $form->setMessenger($container->get('messenger'));
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'upload_csv_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form_state->disableCache();

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#description' => $this->t('Je wordt op de hoogte gebracht eens je data verwerkt is'),
      '#required' => TRUE,
      '#weight' => -10,
    ];
    $form['qid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Qid van je organisatie'),
      '#description' => $this->t('Het Wikidata Qid van jouw instelling of organisatie'),
      '#maxlength' => 64,
      '#size' => 64,
      '#required' => TRUE,
      '#weight' => -9,
    ];
    $form['csv'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Jouw CSV'),
      '#upload_location' => 'public://queue/',
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
      ],
      '#required' => TRUE,
      '#unique' => FALSE,
      '#weight' => 10,
    ];

    $form['step'] = [
      '#type' => 'hidden',
      '#default_value' => 1,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#weight' => 90,
    ];

    // Get data if user comes from back link.
    $currentUserDataId = $this->currentRequest->query->get('did', FALSE);
    $valid = $this->currentRequest->query->get('valid', FALSE);
    if ($currentUserDataId !== FALSE) {
      if ($valid === FALSE) {
        $query = $this->database->select('errors', 'e');
      }
      else {
        $query = $this->database->select('valid', 'e');
      }

      // Add extra detail to this query object: a condition, fields and a range.
      $query->condition('e.did', $currentUserDataId);
      $query->fields('e', ['email', 'qid']);
      $query->range(0, 1);
      $result = $query->execute();
      $data = $result->fetch();

      if ($data !== FALSE) {
        $form['email']['#default_value'] = $data->email;
        $form['qid']['#default_value'] = $data->qid;
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $step = (int) $form_state->getValue('step');

    $fileId = $form_state->getValue('csv')[0];
    $file = $this->fileStorage->load($fileId);
    if ($file) {
      $uri = $file->get('uri')->value;
      $url = $this->fileSystem->realpath($uri);

      $generalData = [
        'time' => strtotime('now'),
        'email' => $form_state->getValue('email'),
        'qid' => $form_state->getValue('qid'),
        'dataset_qid' => $form_state->getValue('dataset_qid'),
        'filePath' => $url,
        'fileId' => $fileId,
        'write' => (int) $form_state->getValue('write'),
      ];
      $operations = [];
      $row = 0;

      // Validate the rows.
      if (($handle = fopen($url, 'r')) !== FALSE) {
        $delimiter = $this->validator->getDelimiter($handle);
        while (($data = fgetcsv($handle, 0, $delimiter)) !== FALSE) {
          // Skip first line.
          if ($row !== 0) {
            $operations[] = [
              'validate_row',
              [
                [
                  'generalData' => $generalData,
                  'rowNumber' => $row - 1,
                  'row' => $data,
                  'step' => $step,
                ],
                $this->t('Validating', ['@operation' => $row - 1]),
              ],
            ];
          }
          $row++;
        }
        fclose($handle);
      }

    }

    $batch = [
      'title' => $this->t('Validating', ['@num' => count($operations)]),
      'operations' => $operations,
      'finished' => 'validate_finished',
      'file' => $this->moduleExtensionList->getPath('general') . '/includes/validate.batch.inc',
    ];
    batch_set($batch);
  }

}
