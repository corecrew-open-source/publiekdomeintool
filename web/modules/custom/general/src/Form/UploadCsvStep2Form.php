<?php

namespace Drupal\general\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * CSV Upload form - step 2.
 */
class UploadCsvStep2Form extends UploadCsvForm {

  /**
   * The WikiData settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig|null
   */
  protected ?ImmutableConfig $wikiDataSettings;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $config_factory = $container->get('config.factory');
    if ($config_factory instanceof ConfigFactoryInterface) {
      $instance->wikiDataSettings = $config_factory->get('general.wikidata.settings');
    }
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'upload_csv_step2_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    $form['step']['#default_value'] = 2;

    $form['dataset_qid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Qid van je dataset of database'),
      '#description' => $this->t('Het Wikidata Qid van je dataset of database. Indien niet beschikbaar, vul dan het Qid in van de databasesoftware die je gebruikt.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#required' => FALSE,
    ];

    $writing_enabled = (bool) $this->wikiDataSettings->get('writing_enabled');

    $form['write'] = [
      '#type' => 'checkbox',
      '#title' => $writing_enabled ? $this->t('Schrijf data naar Wikidata') : Markup::create('<span style="color:gray">Schrijf data naar Wikidata (tijdelijk niet beschikbaar)</span> '),
      '#default_value' => $writing_enabled ? 1 : 0,
    ];
    if (!$writing_enabled) {
      $form['write']['#disabled'] = 'disabled';
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $writing_enabled = (bool) $this->wikiDataSettings->get('writing_enabled');
    if (!$writing_enabled) {
      // Always set 'Writing data to wikidata' to false.
      $form_state->setValue('write', 0);
    }
  }

}
