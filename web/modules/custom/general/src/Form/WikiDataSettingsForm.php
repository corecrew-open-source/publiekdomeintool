<?php

namespace Drupal\general\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for WikiData.
 */
class WikiDataSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'general_wikidata_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'general.wikidata.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('general.wikidata.settings');

    $form['writing_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable writing to WikiData'),
      '#default_value' => $config->get("writing_enabled"),
      '#description' => $this->t('Enables or disables writing to Wikidata'),
    ];

    $form['wikidata_api_environment'] = [
      '#type' => 'radios',
      '#title' => $this->t('WikiData API environment'),
      '#default_value' => $config->get("wikidata_api_environment"),
      '#description' => $this->t('The WikiData API environment'),
      '#options' => [
        'test' => $this->t('Test'),
        'live' => $this->t('Live'),
      ],
    ];

    $form['wikidata_api_credentials'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('WikiData API credentials'),
    ];

    $form['wikidata_api_credentials']['wikidata_api_credentials_user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $config->get("wikidata_api_credentials.user"),
    ];

    $form['wikidata_api_credentials']['wikidata_api_credentials_pass'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#default_value' => $config->get("wikidata_api_credentials.pass"),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('general.wikidata.settings')
      ->set('writing_enabled', $form_state->getValue('writing_enabled'))
      ->set('wikidata_api_environment', $form_state->getValue('wikidata_api_environment'))
      ->set('wikidata_api_credentials.user', $form_state->getValue('wikidata_api_credentials_user'))
      ->set('wikidata_api_credentials.pass', $form_state->getValue('wikidata_api_credentials_pass'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
