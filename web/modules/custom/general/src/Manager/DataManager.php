<?php

namespace Drupal\general\Manager;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\general\Service\EntityHelper;

/**
 * Data manager service.
 */
class DataManager extends DataManagerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected EntityTypeManager $entityTypeManager;

  /**
   * DataManager constructor.
   *
   * @param \Drupal\general\Service\EntityHelper $entity_helper
   *   The entity helper service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityHelper $entity_helper, EntityTypeManager $entity_type_manager) {
    parent::__construct($entity_helper);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get email content.
   */
  public function getEmailContent($mail) {
    $nids = [
      'process_step_1_finished' => 6,
      'process_step_2_finished' => 9,
    ];
    return $this->setData([$nids[$mail]], 'node');
  }

  /**
   * Get CSV.
   */
  public function getCsv($did) {
    $query = $this->entityTypeManager->getStorage('csv')->getQuery();
    $query->condition('field_did', $did);
    $query->accessCheck(FALSE);
    $entity_ids = $query->execute();
    return $this->setData($entity_ids, 'csv');
  }

}
