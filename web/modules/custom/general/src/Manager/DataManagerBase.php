<?php

namespace Drupal\general\Manager;

use Drupal\Core\Entity\EntityInterface;
use Drupal\general\Service\EntityHelper;

/**
 * Base class for DataManager classes.
 */
abstract class DataManagerBase {

  /**
   * The data.
   *
   * @var array
   */
  protected array $data;

  /**
   * The entity type.
   *
   * @var string
   */
  protected string $type;

  /**
   * The entity helper.
   *
   * @var \Drupal\general\Service\EntityHelper
   */
  protected EntityHelper $entityHelper;

  /**
   * DataManagerBase constructor.
   *
   * @param \Drupal\general\Service\EntityHelper $entityHelper
   *   The entity helper.
   */
  public function __construct(EntityHelper $entityHelper) {
    $this->entityHelper = $entityHelper;
  }

  /**
   * Returns the entity type.
   *
   * @return string
   *   The entity type.
   */
  public function getType(): string {
    return $this->type;
  }

  /**
   * Sets the entity type.
   *
   * @param string $type
   *   The entity type.
   */
  public function setType(string $type): void {
    $this->type = $type;
  }

  /**
   * Returns the data.
   *
   * @return array
   *   The data.
   */
  public function getData(): array {
    return $this->data;
  }

  /**
   * Sets the data.
   *
   * @param array $data
   *   The data.
   * @param string $type
   *   The entity type.
   *
   * @return $this
   */
  public function setData(array $data, string $type = 'node') {
    $this->data = $data;
    $this->setType($type);

    return $this;
  }

  /**
   * Returns the entities provided by the data.
   *
   * @param string|null $language
   *   The language.
   * @param array $context
   *   The context.
   *
   * @return array|null
   *   An array of entities or null when nothing is found.
   */
  public function getEntities(string $language = NULL, array $context = []): ?array {
    $ids = $this->getData();
    $type = $this->getType();

    if (!empty($ids)) {
      return $this->entityHelper->loadMultiple($type, $ids, $language, $context);
    }

    return NULL;
  }

  /**
   * Returns a single entity.
   *
   * @param string|null $language
   *   The language.
   * @param array $context
   *   The context.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity or null when nothing is found.
   */
  public function getEntity(string $language = NULL, array $context = []): ?EntityInterface {
    $entities = $this->getEntities($language, $context);

    if (!empty($entities) && count($entities) > 0) {
      return array_shift($entities);
    }

    return NULL;
  }

  /**
   * Returns the views of the available data.
   *
   * @param string $view_mode
   *   The view mode.
   * @param string|null $language
   *   The language.
   * @param bool $reset
   *   Reset.
   * @param bool $group_views
   *   Group views.
   *
   * @return array|null
   *   The entity views or null when nothing is found.
   */
  public function getEntityViews(string $view_mode = 'full', string $language = NULL, bool $reset = FALSE, bool $group_views = TRUE) {
    $entities = $this->getEntities();

    if (!empty($entities)) {
      return $this->entityHelper->viewMultiple($entities, $view_mode, $language, $reset, $group_views);
    }

    return NULL;
  }

  /**
   * Returns the view of a single entity.
   *
   * @param string $view_mode
   *   The view mode.
   * @param string|null $language
   *   The language.
   * @param bool $reset
   *   Reset.
   *
   * @return array
   *   View of an entity or null when nothing is found.
   */
  public function getEntityView(string $view_mode = 'full', string $language = NULL, bool $reset = FALSE) {
    $entity = $this->getEntity();

    if (!empty($entity)) {
      return $this->entityHelper->view($entity, $view_mode, $language, $reset);
    }

    return NULL;
  }

  /**
   * Returns the views as an array of single view instances.
   *
   * @param string $view_mode
   *   The view mode.
   * @param string|null $language
   *   The language.
   * @param bool $reset
   *   Reset.
   * @param bool $group_views
   *   Group views.
   *
   * @return array|null
   *   Single entity views or null when nothing is found.
   */
  public function getSingleEntityViews($view_mode = 'full', string $language = NULL, bool $reset = FALSE, bool $group_views = FALSE) {
    return $this->getEntityViews($view_mode, $language, $reset, $group_views);
  }

}
