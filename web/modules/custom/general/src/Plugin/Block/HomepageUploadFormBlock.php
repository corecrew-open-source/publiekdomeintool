<?php

namespace Drupal\general\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilder;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\general\Form\UploadCsvForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'HomepageUploadFormBlock' block.
 *
 * @Block(
 *  id = "homepage_upload_form_block",
 *  admin_label = @Translation("Homepage upload form block"),
 * )
 */
class HomepageUploadFormBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Construct a HomepageUploadFormBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilder $formBuilder
   *   The form builder service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected FormBuilder $formBuilder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return $this->formBuilder->getForm(UploadCsvForm::class);
  }

}
