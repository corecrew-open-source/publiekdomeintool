<?php

namespace Drupal\general\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'QueueBlock' block.
 *
 * @Block(
 *  id = "queue_block",
 *  admin_label = @Translation("Queue block"),
 * )
 */
class QueueBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a QueueBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Queue\QueueFactory $queue
   *   The queue factory service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected QueueFactory $queue) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('queue')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $return = [
      '#theme' => 'queue',
    ];
    for ($i = 1; $i <= 2; $i++) {
      $queue = $this->queue->get('csv_step_' . $i);
      $return['#step' . $i] = $queue->numberOfItems();
    }

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): int {
    return 0;
  }

}
