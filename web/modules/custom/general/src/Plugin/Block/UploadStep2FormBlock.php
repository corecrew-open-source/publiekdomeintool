<?php

namespace Drupal\general\Plugin\Block;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\general\Form\UploadCsvStep2Form;

/**
 * Provides a 'UploadStep2FormBlock' block.
 *
 * @Block(
 *  id = "upload_step2_form_block",
 *  admin_label = @Translation("Upload step2 form block"),
 * )
 */
class UploadStep2FormBlock extends HomepageUploadFormBlock implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return $this->formBuilder->getForm(UploadCsvStep2Form::class);
  }

}
