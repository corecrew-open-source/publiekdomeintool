<?php

namespace Drupal\general\Plugin\QueueWorker;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\general\Api\WikiDataEndpoint;
use Drupal\general\CsvErrorRowEnum;
use Drupal\general\CsvRowEnum;
use Drupal\general\Data\BatchGeneralData;
use Drupal\general\Entity\Csv;
use Drupal\general\Service\ProcessorHelper;
use Drupal\general\Service\ReconciliationHelper;
use Drupal\general\Service\SSLHelper;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy\TermStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Save queue item in a node.
 *
 * To process the queue items whenever Cron is run,
 * we need a QueueWorker plugin with an annotation witch defines
 * to witch queue it applied.
 *
 * @QueueWorker(
 *   id = "csv_step_1",
 *   title = @Translation("Import Content From CSV files (step 1)"),
 *   cron = {"time" = 840}
 * )
 */
class ProcessCsv extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The taxonomy term storage service.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected TermStorageInterface $termStorage;

  /**
   * The WikiData endpoint.
   *
   * @var \Drupal\general\Api\WikiDataEndpoint
   */
  protected WikiDataEndpoint $wikiDataEndpoint;

  /**
   * Constructs a ProcessCsv queue worker class.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\general\Service\SSLHelper $sslHelper
   *   The SSL helper service.
   * @param \Drupal\general\Service\ReconciliationHelper $reconciliationHelper
   *   The reconciliation helper service.
   * @param \Drupal\general\Service\ProcessorHelper $processorHelper
   *   The processor helper service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected Connection $database, protected SSLHelper $sslHelper, protected ReconciliationHelper $reconciliationHelper, protected ProcessorHelper $processorHelper, protected EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->termStorage = $entity_type_manager->getStorage('taxonomy_term');
    $this->wikiDataEndpoint = new WikiDataEndpoint();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('general.sslhelper'),
      $container->get('general.reconciliationhelper'),
      $container->get('general.processor_helper'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem(mixed $item) {
    // Generate the file.
    $generalData = new BatchGeneralData($item['generalData']);
    $processorHelperData = $this->processorHelper->makeFile($item, $generalData);


    // The original row.
    $row = $item['row'];
    $needles = [
      'anoniem',
      'anonymous',
      'onbekend',
    ];

    $newRow[CsvRowEnum::wikidata_link()->value] = '';
    $newRow[CsvRowEnum::instance_of()->value] = '';
    // Get new data to add to the row.
    if ($row[CsvRowEnum::wikidata_id()->value] === '' && preg_match('/' . implode('|', $needles) . '$/i', $row[CsvRowEnum::creator()->value]) !== 1) {
      // Reconcilation api.
      $openRefineData = $this->reconciliationHelper->getReconData($row);
      // If the api returns no data re-add the item to the queue.
      if ($openRefineData['status'] === FALSE) {
        // Add an error message to the CSV when no result is found or there was
        // a server error.
        $newRow = $row;
        $newRow[CsvErrorRowEnum::step1_error_code()->value] = $openRefineData['error_code'];
        $newRow[CsvErrorRowEnum::step1_error_message()->value] = "Dit item kon niet verrijkt worden door Wikidata.\nFoutmelding: " . $openRefineData['message'] . ".";

        // Fill in blank columns in the new row.
        // Add a buffer of 1 column because Step 2 will add another column later
        $rowLength = count($newRow) +1;
        for ($i = 0; $i < $rowLength; $i++) {
          $key = $i;
          if ($key >= 8) {
            $key = $i + 1;
          }
          if (!isset($newRow[$key])) {
            $newRow[$key] = $row[$i];
          }
        }

        // Sort the row by index keys.
        ksort($newRow);

        $realpath = $this->processorHelper->addRowToFile($processorHelperData['uri'], $newRow, $item['step'], $generalData);

        // If number of rows in new CSV = number of rows in old file ($generalData['numberOfRows']) notify the user.
        $this->processorHelper->sendMailOnComplete($item, $realpath, $processorHelperData['entity'], $generalData);

        return;
      }

      $artists = [];
      foreach ($openRefineData as $artist) {
        $add = TRUE;
        $wikidata = $this->sslHelper->getSslData($this->wikiDataEndpoint->getEndpointBaseUrl() . '/wiki/Special:EntityData/' . $artist->id . '.json');
        $wikidata = json_decode($wikidata, TRUE);
        $entity = $wikidata['entities'][$artist->id];
        if (isset($entity['claims'][$this->wikiDataEndpoint->getProperty('occupation')])) {
          $categories = $entity['claims'][$this->wikiDataEndpoint->getProperty('occupation')];
          // Strip the athletes & politicians.
          foreach ($categories as $category) {
            if ($add) {
              $category = $category['mainsnak']['datavalue']['value']['id'];
              $types = [
                'athletes',
                'politicians',
              ];
              $query = $this->database->select('sparql', 'sparql');
              $query->condition('sparql.qid', $category);
              $query->condition('sparql.type', $types, 'IN');
              $query->fields('sparql', ['type']);
              $query->range(0, 1);
              $result = $query->execute();
              $data = $result->fetch();
              if ($data !== FALSE) {
                $add = FALSE;
              }
            }
          }
        }
        else {
          $categories = NULL;
          $add = FALSE;
        }

        // Only continue if its not already stripped because athlete or
        // politician.
        if ($add && $row[CsvRowEnum::object_name()->value] !== '') {
          // Add 'Objectnaam' under taxonmy 'Geen categorie' if categorie
          // not added.
          $catName = strtolower($row[CsvRowEnum::object_name()->value]);
          $vid = 'categories';
          $term_properties = [
            'name' => trim($catName),
            'vid' => $vid,
          ];
          $terms = $this->termStorage->loadByProperties($term_properties);
          // If category doesn't exist yet, create it with the 'Onbepaald'
          // category as parent.
          if (empty($terms)) {
            // Get the term ID for category 'Onbepaald'.
            $cat_name = 'Onbepaald';
            $cat_arr = $this->termStorage->loadByProperties(['name' => $cat_name]);
            $category_undefined = reset($cat_arr);
            $parent_tid_none = $category_undefined->id();

            $createData = [
              'vid' => $vid,
              'name' => $catName,
            ];
            $term = $this->termStorage->create($createData);
            $term->enforceIsNew();
            $term->parent = $parent_tid_none;
            $term->save();

            $parentIdQId = 'NONE';
          }
          else {
            $parentId = reset($terms)->parent->target_id;
            $parent_term = $this->termStorage->load($parentId);
            if ($parent_term instanceof TermInterface) {
              $parentIdQId = $parent_term->get('field_qid')->getValue()[0]['value'];
            }
          }

          if (isset($categories)) {
            if ($parentIdQId === 'NONE') {
              // Only leave the Qids under the categories artists or a subclass
              // of it (wikidata).
              $add = $this->checkIfHasArtist($categories);
            }
            else {
              // Only add if the artist has the parent Category from the
              // taxonomy or a subclass of it in wikidata.
              $add = FALSE;
              $matchOnCat = FALSE;
              foreach ($categories as $category) {
                if ($add === FALSE) {
                  $category = $category['mainsnak']['datavalue']['value']['id'];
                  $query = $this->database->select('sparql', 'sparql');
                  $query->condition('sparql.qid', $category);
                  $query->condition('sparql.type', $parentIdQId);
                  $query->fields('sparql', ['type']);
                  $query->range(0, 1);
                  $result = $query->execute();
                  $data = $result->fetch();
                  if ($parentIdQId === $category || $data !== FALSE) {
                    $add = TRUE;
                    if ($matchOnCat === FALSE) {
                      $matchOnCat = TRUE;
                    }
                  }
                }
              }

              if ($matchOnCat === FALSE) {
                $add = $this->checkIfHasArtist($categories);
              }
            }
          }
        }

        if ($add && $row[CsvRowEnum::object_name()->value] === '' && isset($categories)) {
          $add = $this->checkIfHasArtist($categories);
        }

        if ($add) {
          if ($artist->score >= 55) {
            $artists[$artist->id] = 'http://wikidata.org/wiki/' . $artist->id;
          }
        }
      }

      // Check for 'organisatie, een groep of een individu' if there is no QID.
      if (count($artists) > 0) {
        // Add data to file row.
        $newRow[CsvRowEnum::wikidata_id()->value] = implode(',', array_keys($artists)) . '';
        $newRow[CsvRowEnum::wikidata_link()->value] = implode(',', $artists) . '';
      }

      $needles = [
        'bvba',
        ' nv',
        ' vzw',
        'organisatie',
        'groep',
        'Sprl',
        'partners',
        '&',
        'studio',
        ' Co',
        'N.V.',
        'et fils',
        'en zoon',
        'buro',
        'bureau',
        'en zonen',
      ];
      if (preg_match('/' . implode('$|', $needles) . '/i', $row[CsvRowEnum::creator()->value]) === 1) {
        $newRow[CsvRowEnum::instance_of()->value] = 'Organisatie';
      }
      else {
        $newRow[CsvRowEnum::instance_of()->value] = 'Individu';
      }
    }

    if ($row[CsvRowEnum::date_of_death()->value] !== '') {
      $newRow[CsvRowEnum::copyright_status()->value] = 'Gekend';
    }
    elseif ($row[CsvRowEnum::floruit()->value] !== '' || isset($newRow[CsvRowEnum::wikidata_id()->value]) || $row[CsvRowEnum::date_of_birth()->value] !== '') {
      $newRow[CsvRowEnum::copyright_status()->value] = 'Mogelijk gekend';
    }
    else {
      $newRow[CsvRowEnum::copyright_status()->value] = 'Niet gekend';
    }

    // Fill the blanks in the new row.
    for ($i = 0; $i < count($row); $i++) {
      $key = $i;
      if ($key >= 8) {
        $key = $i + 1;
      }
      if (!isset($newRow[$key])) {
        $newRow[$key] = $row[$i];
      }
    }
    ksort($newRow);

    // Add the row to the new CSV file.
    $realpath = $this->processorHelper->addRowToFile($processorHelperData['uri'], $newRow, $item['step'], $generalData);

    // If number of rows in new CSV = number of rows in old file
    // ($generalData->getNumberOfRows) notify the user.
    $this->processorHelper->sendMailOnComplete($item, $realpath, $processorHelperData['entity'], $generalData);
  }

  /**
   * Check if has artist.
   *
   * @param array $categories
   *   The list of categories.
   *
   * @return bool
   *   Whether it has an artist.
   */
  private function checkIfHasArtist(array $categories): bool {
    $hasArtist = 0;
    foreach ($categories as $category) {
      $category = $category['mainsnak']['datavalue']['value']['id'];
      $query = $this->database->select('sparql', 'sparql');
      $query->condition('sparql.qid', $category);
      $query->condition('sparql.type', 'artists');
      $query->fields('sparql', ['type']);
      $query->range(0, 1);
      $result = $query->execute();
      $data = $result->fetch();
      if ($data !== FALSE) {
        $hasArtist++;
      }
    }
    if ($hasArtist === 0) {
      return FALSE;
    }
    return TRUE;
  }

}
