<?php

namespace Drupal\general\Plugin\QueueWorker;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\general\Api\WikiDataEndpoint;
use Drupal\general\CsvErrorRowEnum;
use Drupal\general\CsvRowEnum;
use Drupal\general\Data\BatchGeneralData;
use Drupal\general\Entity\WikidataUpdate;
use Drupal\general\Service\ProcessorHelper;
use Drupal\general\Service\SSLHelper;
use Drupal\general\Service\Validator;
use Drupal\general\Service\WikidataHelper;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy\TermStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Save queue item in a node.
 *
 * To process the queue items whenever Cron is run,
 * we need a QueueWorker plugin with an annotation witch defines
 * to witch queue it applied.
 *
 * @QueueWorker(
 *   id = "csv_step_2",
 *   title = @Translation("Import Content From CSV files (step 2)"),
 *   cron = {"time" = 840}
 * )
 */
class ProcessCsvStep2 extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The taxonomy term storage service.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected TermStorageInterface $termStorage;

  /**
   * The WikiData settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $wikiDataSettings;

  /**
   * The WikiData endpoint.
   *
   * @var \Drupal\general\Api\WikiDataEndpoint
   */
  protected WikiDataEndpoint $wikiDataEndpoint;

  /**
   * Constructs a ProcessCsv step 2 queue worker class.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\general\Service\Validator $validator
   *   The validator service.
   * @param \Drupal\general\Service\SSLHelper $sslHelper
   *   The SSL helper service.
   * @param \Drupal\general\Service\ProcessorHelper $processorHelper
   *   The processor helper service.
   * @param \Drupal\general\Service\WikidataHelper $wikidataHelper
   *   The Wikidata helper service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   The logger service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              protected Validator $validator,
                              protected SSLHelper $sslHelper,
                              protected ProcessorHelper $processorHelper,
                              protected WikidataHelper $wikidataHelper,
                              protected LoggerChannelFactory $logger,
                              EntityTypeManagerInterface $entity_type_manager,
                              ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->termStorage = $entity_type_manager->getStorage('taxonomy_term');
    $this->wikiDataSettings = $config_factory->get('general.wikidata.settings');
    $this->wikiDataEndpoint = new WikiDataEndpoint($this->wikiDataSettings->get('wikidata_api_environment'));
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('general.validator'),
      $container->get('general.sslhelper'),
      $container->get('general.processor_helper'),
      $container->get('general.wikidata_helper'),
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item) {
    // Generate the file.
    $generalData = new BatchGeneralData($item['generalData']);
    $processorHelperData = $this->processorHelper->makeFile($item, $generalData);

    // Get row from the Queue item.
    $row = $item['row'];
    $row[CsvRowEnum::copyright_status()->value] = '';
    $row[CsvRowEnum::public_year()->value] = '';
    $row[CsvRowEnum::copyright_representative()->value] = '';
    $row[CsvErrorRowEnum::step2_error_code()->value] = '';
    $row[CsvErrorRowEnum::step2_error_message()->value] = '';

    // Enrich row with Wikidata data + write to Wikidata if requested.
    $wikidata_success = $this->wikiDataEnrichment($row, $generalData);

    if (!$wikidata_success) {
      $filepath_arr = explode('/', $item["generalData"]["filePath"]);
      $filename = end($filepath_arr);
      $this->logger->get('csv_step_2')->error("No data found for QID %qid.
      The item being processed was:
      <li>Filename: %filename</li>
      <li>Row number: %row_num</li>", [
        '%qid' => $generalData->getOrganisationQid(),
        '%filename' => $filename,
        '%row_num' => $item["rowNumber"],
      ]);
    }

    // Enrich row with dates and copyright info.
    $this->enrichDatesAndCopyrightInfo($row);

    // Add the row to the new CSV file.
    $realpath = $this->processorHelper->addRowToFile($processorHelperData['uri'], $row, $item['step'], $generalData);

    // If number of rows in new CSV = number of rows in old file
    // ($generalData->getNumberOfRows) notify the user.
    $this->processorHelper->sendMailOnComplete($item, $realpath, $processorHelperData['entity'], $generalData);
  }

  /**
   * Enriches the row with Wikidata logic and write to Wikidata if relevant.
   *
   * @param array $row
   *   The row to enrich.
   * @param \Drupal\general\Data\BatchGeneralData $generalData
   *   The general batch data.
   *
   * @return bool
   *   Whether Wikidata enrichment was succesful or not.
   */
  protected function wikiDataEnrichment(array &$row, BatchGeneralData $generalData): bool {
    $writtenValues = [];
    $isNew = NULL;
    $isAnon = FALSE;
    $occupation_term = NULL;

    // Get new data to add to the row.
    // Check if we have a valid Wikidata ID.
    if ($row[CsvRowEnum::wikidata_id()->value] !== '') {
      // We have a valid Wikidata ID. Enrich data based on that.
      $isNew = FALSE;
      $wikidata = $this->sslHelper->getSslData($this->wikiDataEndpoint->getEndpointBaseUrl() . '/wiki/Special:EntityData/' . $row[CsvRowEnum::wikidata_id()->value] . '.json');
      $wikidata = json_decode($wikidata, TRUE);
      if ($wikidata == NULL) {
        return FALSE;
      }
      $entity = $wikidata['entities'][$row[CsvRowEnum::wikidata_id()->value]];
      foreach ($this->wikiDataEndpoint->getRowPropertyMapping() as $rowKey => $propertyId) {
        if (isset($entity['claims'][$propertyId])) {
          $propertyData = $entity['claims'][$propertyId];
          $propertyValue = $propertyData[0]['mainsnak']['datavalue'];
          if ($propertyValue['type'] === 'wikibase-entityid') {
            if ($propertyId === $this->wikiDataEndpoint->getProperty('copyright status as a creator')) {
              // Ignore copyright info coming from Wikidata, as we will
              // calculate it ourselves later on in enrichDatesAndCopyrightInfo.
              $row[$rowKey] = '';
            }
            else {
              $row[$rowKey] = $propertyValue['value']['id'];
            }
            $writtenValues[] = $rowKey;
          }
          if ($propertyValue['type'] === 'time') {
            if ($row[$rowKey] === '') {
              $writtenValues[] = $rowKey;
              $row[$rowKey] = (new \DateTime($propertyValue['value']['time']))->format('Y');
            }
          }
          if ($propertyValue['type'] === 'string') {
            if ($row[$rowKey] === '') {
              $writtenValues[] = $rowKey;
              $row[$rowKey] = $propertyValue['value'];
            }
          }
        }
      }
    }
    else {
      // We do not have a valid Wikidata ID. Load or create it.
      $needles = [
        'anoniem',
        'anonymous',
        'anonieme meester',
        'onbekend',
      ];
      if (preg_match('/' . implode('|', $needles) . '$/i', $row[CsvRowEnum::creator()->value]) === 1) {
        $isAnon = TRUE;
      }

      // Write the new data to wikidata.
      if ($generalData->writeToWikidata() && $isAnon === FALSE) {
        // Check if item was previously created.
        $query = \Drupal::entityQuery('wikidata_update')
          ->condition('field_property', 'P0')
          ->condition('field_value', $row[CsvRowEnum::creator()->value])
          ->condition('field_qid_user', $generalData->getOrganisationQid())
          ->condition('field_email', $generalData->getEmail());

        $updateItems = $query->execute();

        if (!empty($updateItems)) {
          // $updateItems = $query->execute();
          $updateItemId = reset($updateItems);
          $updateItem = WikidataUpdate::load($updateItemId);
          $newItemid = $updateItem->get('field_qid')->getString();
        }
        else {
          // Create new item in Wikidata.
          $occupation_term = $this->getOccupationTerm($row);
          $newItemid = $this->wikidataHelper->createNewItem($row, $generalData, $occupation_term);
          if (!$newItemid) {
            // Update flag to avoid writing to WikiData, if data is not
            // considered valid for new item creation.
            $generalData->setWriteToWikidata(FALSE);
          }
        }

        if ($newItemid) {
          $isNew = TRUE;
          $row[CsvRowEnum::wikidata_id()->value] = '' . $newItemid;
          $row[CsvRowEnum::wikidata_link()->value] = 'http://wikidata.org/wiki/' . $newItemid;
        }
      }
    }

    // Write the new data to Wikidata, if enabled and validation passed.
    if ($generalData->writeToWikidata() && $isAnon === FALSE) {
      $this->writeDataToWikidata($row, $generalData, $writtenValues, $isNew, $occupation_term);
    }
    return TRUE;
  }

  /**
   * Get the author occupation term based on row data.
   *
   * @param array $row
   *   The row data to use to retrieve occupation term.
   *
   * @return \Drupal\taxonomy\TermInterface|null
   *   The taxonomy term indicating the occupation of this row.
   */
  protected function getOccupationTerm(array $row): ?TermInterface {
    $occupation_term = NULL;
    if ($row[CsvRowEnum::object_name()->value] !== '' && strtolower($row[CsvRowEnum::instance_of()->value]) !== 'organisatie') {
      $catName = strtolower($row[CsvRowEnum::object_name()->value]);
      $term_properties = [
        'name' => trim($catName),
        'vid' => 'categories',
      ];
      $terms = $this->termStorage->loadByProperties($term_properties);
      if (!empty($terms)) {
        $parentId = reset($terms)->parent->target_id;
        $parent_term = $this->termStorage->load($parentId);
        if ($parent_term instanceof TermInterface) {
          $occupation_term = $parent_term;
        }
      }
    }
    return $occupation_term;
  }

  /**
   * Write CSV data to Wikidata.
   *
   * @param array $row
   *   The row data.
   * @param \Drupal\general\Data\BatchGeneralData $generalData
   *   The general data for this batch.
   * @param array $writenValues
   *   The written values.
   * @param bool $isNew
   *   Whether item is new.
   * @param \Drupal\taxonomy\TermInterface|null $occupation_term
   *   The occupation term.
   *
   * @throws \Mediawiki\Api\UsageException
   */
  protected function writeDataToWikidata(array &$row, BatchGeneralData $generalData, array $writenValues, bool $isNew, ?TermInterface $occupation_term = NULL) {
    // Check if writing to WikiData is enabled. If not, bail out.
    $writing_enabled = (bool) $this->wikiDataSettings->get('writing_enabled');
    if (!$writing_enabled) {
      return;
    }

    $qid = trim($row[CsvRowEnum::wikidata_id()->value]);
    if (!empty($qid)) {
      $save_needed = FALSE;
      $this->wikidataHelper->prepareNewRevision($qid);

      // Type auteur.
      $types = $this->wikiDataEndpoint->getAuthorTypes();

      $organisationExists = FALSE;
      if ($isNew === FALSE && strtolower($row[CsvRowEnum::instance_of()->value]) === 'organisatie') {
        $organisationExists = TRUE;
      }

      if ($organisationExists !== TRUE) {
        $saved = $this->wikidataHelper->createStatement($qid, $this->wikiDataEndpoint->getProperty('instance of'), $types[$row[CsvRowEnum::instance_of()->value]], $row, $generalData);
        $save_needed = $saved ? TRUE : $save_needed;
      }

      // Use type of object to optionally set occupation.
      if (!$occupation_term) {
        $occupation_term = $this->getOccupationTerm($row);
      }
      if ($occupation_term) {
        $parentIdQId = $occupation_term->get('field_qid')->getValue()[0]['value'];
        if ($parentIdQId !== 'NONE') {
          $saved = $this->wikidataHelper->createStatement($qid, $this->wikiDataEndpoint->getProperty('occupation'), $parentIdQId, $row, $generalData);
          $save_needed = $saved ? TRUE : $save_needed;
        }
      }

      // Geboortedatum.
      if ($row[CsvRowEnum::date_of_birth()->value] !== '' && !in_array(CsvRowEnum::date_of_birth()->value, $writenValues)) {
        $saved = $this->wikidataHelper->createStatement($qid, $this->wikiDataEndpoint->getProperty('date of birth'), $row[CsvRowEnum::date_of_birth()->value], $row, $generalData);
        $save_needed = $saved ? TRUE : $save_needed;
      }

      // Sterfdatum.
      if ($row[CsvRowEnum::date_of_death()->value] !== '' && !in_array(CsvRowEnum::date_of_death()->value, $writenValues)) {
        $saved = $this->wikidataHelper->createStatement($qid, $this->wikiDataEndpoint->getProperty('date of death'), $row[CsvRowEnum::date_of_death()->value], $row, $generalData);
        $save_needed = $saved ? TRUE : $save_needed;
      }
      elseif ($row[CsvRowEnum::floruit()->value] !== '') {
        // Creatiedatum (floruit).
        $saved = $this->wikidataHelper->createStatement($qid, $this->wikiDataEndpoint->getProperty('floruit'), $row[CsvRowEnum::floruit()->value], $row, $generalData);
        $save_needed = $saved ? TRUE : $save_needed;
      }

      // VIAF.
      if ($row[CsvRowEnum::viaf()->value] !== '' && !in_array(CsvRowEnum::viaf()->value, $writenValues)) {
        $saved = $this->wikidataHelper->createStatement($qid, $this->wikiDataEndpoint->getProperty('viaf'), $row[CsvRowEnum::viaf()->value], $row, $generalData);
        $save_needed = $saved ? TRUE : $save_needed;
      }

      // RKD.
      if ($row[CsvRowEnum::rkd()->value] !== '' && !in_array(CsvRowEnum::rkd()->value, $writenValues)) {
        $saved = $this->wikidataHelper->createStatement($qid, $this->wikiDataEndpoint->getProperty('rkd'), $row[CsvRowEnum::rkd()->value], $row, $generalData);
        $save_needed = $saved ? TRUE : $save_needed;
      }

      // ULAN.
      if ($row[CsvRowEnum::ulan()->value] !== '' && !in_array(CsvRowEnum::ulan()->value, $writenValues)) {
        $saved = $this->wikidataHelper->createStatement($qid, $this->wikiDataEndpoint->getProperty('ulan'), $row[CsvRowEnum::ulan()->value], $row, $generalData);
        $save_needed = $saved ? TRUE : $save_needed;
      }

      // Werken in collectie.
      $saved = $this->wikidataHelper->createStatement($qid, $this->wikiDataEndpoint->getProperty('has works in the collection'), $generalData->getOrganisationQid(), $row, $generalData);
      $save_needed = $saved ? TRUE : $save_needed;

      // Finally, write the new revision to Wikidata.
      if ($save_needed) {
        $this->wikidataHelper->saveNewRevision();
      }
    }
  }

  /**
   * Enriches the row with dates and copyright info.
   *
   * @param array $row
   *   The row to enrich.
   */
  protected function enrichDatesAndCopyrightInfo(array &$row): void {
    // Get the currentYear.
    $currentYear = (int) (new \DateTime('now'))->format('Y');

    // Anonymous check.
    $isAnon = FALSE;
    $needles = [
      'anoniem',
      'anonymous',
      'anonieme meester',
    ];
    if (preg_match('/' . implode('|', $needles) . '$/i', $row[CsvRowEnum::creator()->value]) === 1) {
      $isAnon = TRUE;
    }

    // Die date.
    if ($row[CsvRowEnum::date_of_death()->value] !== '') {
      $dieYear = $this->validator->convertDateToYear($row[CsvRowEnum::date_of_death()->value]);
      if ($currentYear - (int) $dieYear > 70) {
        $row[CsvRowEnum::copyright_status()->value] = 'Publiek domein';
      }
      else {
        $row[CsvRowEnum::copyright_status()->value] = 'Auteursrechtelijk beschermd';
      }
    }
    elseif ($row[CsvRowEnum::date_of_birth()->value] !== '') {
      // Birth date.
      $birthYear = $this->validator->convertDateToYear($row[CsvRowEnum::date_of_birth()->value]);
      if ($currentYear - $birthYear > 170) {
        $row[CsvRowEnum::copyright_status()->value] = 'Publiek domein';
      }
      elseif ($currentYear - $birthYear <= 170 && $currentYear - $birthYear > 70) {
        $row[CsvRowEnum::copyright_status()->value] = 'Auteursrecht onbekend';
      }
      elseif ($currentYear - $birthYear <= 70) {
        $row[CsvRowEnum::copyright_status()->value] = 'Auteursrechtelijk beschermd';
      }
    }
    elseif ($row[CsvRowEnum::floruit()->value] !== '') {
      // Created date.
      $createdYear = $this->validator->convertDateToYear($row[CsvRowEnum::floruit()->value]);
      if ($currentYear - $createdYear > 170) {
        $row[CsvRowEnum::copyright_status()->value] = 'Publiek domein';
      }
      elseif ($currentYear - $createdYear <= 170 && $currentYear - $createdYear > 70) {
        $row[CsvRowEnum::copyright_status()->value] = 'Auteursrecht onbekend';
        if ($isAnon) {
          $row[CsvRowEnum::copyright_status()->value] = 'Publiek domein';
        }
      }
      elseif ($currentYear - $createdYear <= 70) {
        $row[CsvRowEnum::copyright_status()->value] = 'Auteursrechtelijk beschermd';
      }
    }

    if ($row[CsvRowEnum::copyright_status()->value] === '') {
      $row[CsvRowEnum::copyright_status()->value] = 'Auteursrecht onbekend';
    }

    // Add year when this item will become public domain.
    if (isset($dieYear)) {
      $yearsToGo = $dieYear + 71;
    }
    elseif (isset($birthYear)) {
      $yearsToGo = $birthYear + 171;
    }
    elseif (isset($createdYear)) {
      $yearsToGo = (!$isAnon) ? $createdYear + 171 : $createdYear + 71;
    }

    $row[CsvRowEnum::public_year()->value] = $yearsToGo ?? 'N/A';
  }

}
