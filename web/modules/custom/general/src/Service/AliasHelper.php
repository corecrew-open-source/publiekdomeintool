<?php

namespace Drupal\general\Service;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\path_alias\AliasManagerInterface;

/**
 * The alias helper service.
 */
class AliasHelper {

  /**
   * AliasService constructor.
   *
   * @param \Drupal\Core\Language\LanguageManager $languageManager
   *   The language manager service.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory service.
   * @param \Drupal\path_alias\AliasManagerInterface $aliasManager
   *   The path alias manager.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPathStack
   *   The current path stack.
   */
  public function __construct(protected LanguageManager $languageManager, protected ConfigFactory $configFactory, protected AliasManagerInterface $aliasManager, protected CurrentPathStack $currentPathStack) {}

  /**
   * Add a front slash if the string does not contain it yet.
   *
   * @param string $string
   *   The string to process.
   *
   * @return string
   *   The processed string.
   */
  public function addFrontSlash(string $string): string {
    $firstCharacter = substr($string, 0, 1);
    if ($firstCharacter !== '/') {
      $string = '/' . $string;
    }
    return $string;
  }

  /**
   * Create a renderable URL from a URL object.
   *
   * @param \Drupal\Core\Url $urlObject
   *   The URL object.
   * @param string|null $language
   *   The language ID.
   *
   * @return \Drupal\Core\GeneratedUrl|string
   *   The generated URL.
   */
  public function getUrlFromUrlObject(Url $urlObject, string $language = NULL) {
    if ($urlObject->isRouted() === TRUE) {
      // Unrouted URIs do not have internal representations.
      return $this->getAliasFromInternalPath($urlObject->getInternalPath(), $language);
    }
    else {
      return $urlObject->getUri();
    }
  }

  /**
   * Retrieve a renderable URL from an internal path (ex. /node/1 or node/1)
   *
   * @param string $internal_path
   *   The internal path.
   * @param string|null $language
   *   The language ID.
   *
   * @return string
   *   The alias.
   */
  public function getAliasFromInternalPath(string $internal_path, string $language = NULL): string {
    // Add the front slash if it is missing.
    $internalPath = $this->addFrontSlash($internal_path);

    // Retrieve the language if not provided.
    if ($language === NULL) {
      $langObject = $this->languageManager->getCurrentLanguage();
      $language = $langObject->getId();
    }

    $languagePrefix = $this->configFactory->get('language.negotiation')->get('url')['prefixes'][$language];

    // Add front slash if not provided.
    if (str_starts_with($internalPath, 'node')) {
      $internal_path = '/' . $internal_path;
    }

    $aliasByPath = $this->aliasManager->getAliasByPath($internal_path, $language);
    if ($languagePrefix !== '') {
      $aliasByPath = '/' . $languagePrefix . $aliasByPath;
    }

    return $aliasByPath;
  }

  /**
   * Get the current page URL alias.
   *
   * @param string|null $language
   *   The language ID.
   *
   * @return mixed
   *   The current path alias.
   */
  public function getCurrentPageAlias(string $language = NULL) {
    $currentPageInternalPath = $this->currentPathStack->getPath();
    $urlObject = Url::fromUserInput($currentPageInternalPath);

    return $this->getUrlFromUrlObject($urlObject, $language);
  }

  /**
   * Get the URL from a nid.
   *
   * @param string|int $nid
   *   The node ID.
   * @param string|null $language
   *   The language ID.
   *
   * @return string
   *   The alias.
   */
  public function getAliasFromNid($nid, string $language = NULL): string {
    return $this->getAliasFromInternalPath('/node/' . $nid, $language);
  }

  /**
   * Get the URL from a node object.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node.
   * @param string|null $language
   *   The language ID.
   *
   * @return string
   *   The alias.
   */
  public function getAliasFromNode(Node $node, string $language = NULL): string {
    return $this->getAliasFromNid($node->id(), $language);
  }

  /**
   * Convert a string into a URL object.
   *
   * @param string $string
   *   Pass a string version of a url.
   * @param array $options
   *   An option to pass, this could be a query string ('query'=>[])
   *
   * @return \Drupal\Core\Url|static
   *   Return a Url object.
   */
  public function convertStringToUrl(string $string, array $options = []) {
    $firstChar = substr($string, 0, 1);
    $userInputChars = ['/', '?', '#'];

    // Check if the first character contains one of the listed items to make it
    // a fromUserInput valid string.
    if (in_array($firstChar, $userInputChars)) {
      $url = Url::fromUserInput($string);
    }
    else {
      $url = Url::fromUri($string);
    }

    foreach ($options as $key => $option) {
      $existingOption = $url->getOption($key) !== NULL ? $url->getOption($key) : [];
      $mergedOption = array_merge_recursive($existingOption, $option);
      $url->setOption($key, $mergedOption);
    }

    return $url;
  }

  /**
   * Get the URL from a nid.
   *
   * @param string|int $nid
   *   The node ID.
   *
   * @return string|bool
   *   The URL string.
   */
  public function getPersistentUrlFromNid($nid) {
    $node = Node::load($nid);
    $typeAndCode = $this->getTypeAndCodeOfNode($node);
    if ($node !== NULL && $typeAndCode !== FALSE) {
      return $this->addFrontSlash($typeAndCode['type'] . '/' . $typeAndCode['code']);
    }
    return FALSE;
  }

  /**
   * Get type and code for a given node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   *
   * @return array|bool
   *   The data.
   */
  public function getTypeAndCodeOfNode(NodeInterface $node) {
    $mapArray = [
      'archive' => 'field_reference_code',
      'object' => 'field_object_number',
    ];
    $type = $node->getType();
    if (!isset($mapArray[$type])) {
      return FALSE;
    }
    if (isset($node->get($mapArray[$type])->getValue()[0]) && isset($node->get($mapArray[$type])->getValue()[0]['value'])) {
      $code = $node->get($mapArray[$type])->getValue()[0]['value'];
      // If ($node->hasField('field_is_project') && isset($node->get('field_is_project')->getValue()[0]['value'])
      //        && $node->get('field_is_project')->getValue()[0]['value'] === '1' && $type === 'object') {
      //        $type = 'project';
      //      }.
      return [
        'type' => $type,
        'code' => str_replace(['/', 'BE_653717_'], ['_', ''], $code),
      ];
    }
    else {
      return FALSE;
    }
  }

}
