<?php

namespace Drupal\general\Service;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepository;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\file\Entity\File;

/**
 * Entity helper service.
 */
class EntityHelper {

  /**
   * EntityService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityRepository $entityRepository
   *   The entity repository service.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory service.
   * @param \Drupal\Core\Language\LanguageManager $languageManager
   *   The language manager.
   */
  public function __construct(protected EntityTypeManagerInterface $entityTypeManager, protected EntityRepository $entityRepository, protected ConfigFactory $configFactory, protected LanguageManager $languageManager) {}

  /**
   * Return a certain translation for a nodes objects array.
   *
   * @param array $entities
   *   List of entities.
   * @param string|null $language
   *   The language ID.
   * @param array $context
   *   The context.
   *
   * @return array
   *   List of entities.
   *
   * @internal param $nodes
   */
  public function getEntitiesInCorrectLanguage(array $entities, string $language = NULL, array $context = []): array {
    foreach ($entities as &$entity) {
      $entity = $this->entityRepository->getTranslationFromContext($entity, $language, $context);
    }
    return $entities;
  }

  /**
   * Load an entity by its type and id.
   *
   * @param string $type
   *   The entity type.
   * @param string|int $id
   *   The entity ID.
   * @param string|null $language
   *   The language ID.
   * @param array $context
   *   The context.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The loaded entity.
   */
  public function load(string $type, string|int $id, string $language = NULL, array $context = []): ?EntityInterface {
    $entities = $this->loadMultiple($type, [$id], $language, $context);
    if (isset($entities[$id])) {
      return $entities[$id];
    }
    return NULL;
  }

  /**
   * Load multiple entities by its type and id.
   *
   * @param string $type
   *   The entity type.
   * @param array $ids
   *   The entity IDs.
   * @param string|null $language
   *   The language ID.
   * @param array $context
   *   The context.
   *
   * @return array|null
   *   List of entities.
   */
  public function loadMultiple(string $type, array $ids, string $language = NULL, array $context = []): ?array {
    $controller = $this->entityTypeManager->getStorage($type);
    $entities = $controller->loadMultiple($ids);

    return $this->getEntitiesInCorrectLanguage($entities, $language, $context);
  }

  /**
   * Return a renderable array for an entity id.
   *
   * @param string $type
   *   The entity type.
   * @param string|int $id
   *   The entity ID.
   * @param string $view_mode
   *   The view mode to use.
   * @param string|null $language
   *   The language ID.
   *
   * @return array
   *   The render array.
   */
  public function viewById(string $type, string|int $id, string $view_mode, string $language = NULL) {
    $entity = $this->load($type, $id);
    return $this->view($entity, $view_mode, $language);
  }

  /**
   * Return a renderable array for multiple ids.
   *
   * @param string $type
   *   The entity type.
   * @param array $ids
   *   The entity IDs.
   * @param string $view_mode
   *   The view mode to use.
   * @param string|null $language
   *   The language ID.
   *
   * @return array
   *   The render array.
   */
  public function viewMultipleByIds(string $type, array $ids, string $view_mode, string $language = NULL) {
    $entities = $this->loadMultiple($type, $ids);
    return $this->viewMultiple($entities, $view_mode, $language);
  }

  /**
   * Return a renderable array for an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to view.
   * @param string $view_mode
   *   The view mode to use.
   * @param string|null $language
   *   The language ID.
   * @param bool $reset
   *   Whether to reset.
   *
   * @return array
   *   The render array.
   */
  public function view(EntityInterface $entity, string $view_mode, string $language = NULL, bool $reset = FALSE): array {
    if ($language === NULL) {
      $language = $this->languageManager->getCurrentLanguage()->getId();
    }

    $renderController = $this->entityTypeManager->getViewBuilder($entity->getEntityTypeId());
    if ($reset) {
      $renderController->resetCache([$entity]);
    }

    return $renderController->view($entity, $view_mode, $language);
  }

  /**
   * Returns a renderable array for a multiple entities.
   *
   * @param array|\Drupal\Core\Entity\EntityInterface[] $entities
   *   The entities to view.
   * @param string $view_mode
   *   The view mode to use.
   * @param string|null $language
   *   The language ID.
   * @param bool $reset
   *   Whether to reset.
   * @param bool $group_views
   *   Whether to group views.
   *
   * @return array
   *   The render array.
   */
  public function viewMultiple(array $entities, string $view_mode, string $language = NULL, bool $reset = FALSE, bool $group_views = TRUE): array {
    if ($language === NULL) {
      $language = $this->languageManager->getCurrentLanguage()->getId();
    }

    $renderController = $this->entityTypeManager->getViewBuilder(reset($entities)->getEntityTypeId());
    if ($reset) {
      $renderController->resetCache($entities);
    }

    if ($group_views === FALSE) {
      $returnEntities = [];
      foreach ($entities as $entityId => $entity) {
        $returnEntities[$entityId] = $renderController->view($entity, $view_mode, $language);
      }

      return $returnEntities;
    }

    return $renderController->viewMultiple($entities, $view_mode, $language);
  }

  /**
   * Convert an array of nodes to an array of node field arrays.
   *
   * @param array|\Drupal\node\NodeInterface[] $nodes
   *   List of nodes.
   * @param bool $external_use
   *   Whether to enable external use.
   *
   * @return array
   *   The result array.
   */
  public function convertEntityObjectsToArray(array $nodes, bool $external_use = FALSE): array {
    $entitiesArray = [];
    foreach ($nodes as $entity) {
      $entitiesArray[$entity->get('nid')->value] = $this->convertEntityToArray($entity, $external_use);
    }

    return $entitiesArray;
  }

  /**
   * Convert an entity to an array of fields.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to convert.
   * @param bool $external_use
   *   Whether to enable external use.
   * @param array $excluded_fields
   *   List of fields to exclude.
   * @param array $uuids
   *   List of UUIDs.
   *
   * @return array
   *   An array of fields.
   */
  public function convertEntityToArray(EntityInterface $entity, bool $external_use = FALSE, array $excluded_fields = [], array &$uuids = []): array {
    // If the entity UUID has already been processed, return the previous result
    // This is required to avoid an infinity loop when you would reference the
    // upper level entity in a lower level entity.
    $uuid = $entity->get('uuid')->value;
    if (array_key_exists($uuid, $uuids)) {
      return $uuids[$uuid];
    }

    $entityArray = [];
    foreach ($entity->getFields() as $fieldKey => $field) {

      // Do not process excluded fields.
      if (!in_array($fieldKey, $excluded_fields)) {
        // Retrieve the cardinality to know if this is a multiple or single
        // input field.
        if (method_exists($field->getFieldDefinition(), 'getCardinality')) {
          $cardinality = $field->getFieldDefinition()->getCardinality();
        }
        else {
          $cardinality = $field->getFieldDefinition()
            ->getFieldStorageDefinition()
            ->getCardinality();
        }

        // Retrieve all values from the field.
        $value = $field->getValue();
        $entityArray[$fieldKey] = $value;

        // If external use is enabled, all subentities are rentrieved and
        // converted when possible.
        if ($external_use === TRUE) {

          // Convert all fields to simple arrays.
          if (is_array($entityArray[$fieldKey])) {
            foreach ($entityArray[$fieldKey] as $fieldItemKey => &$item) {

              // Exception for fields using 'value'.
              if (isset($item['value'])) {
                $item = $item['value'];
              }

              // Exception for fields using 'target_id'
              // A check if this is a file needs to be added to provide an
              // absolute link.
              if (isset($item['target_id'])) {

                // Check if the target_id concerns a file, if so, add an
                // absolute link.
                $type = $field->getFieldDefinition()->getType();
                if ($type === 'file') {
                  $file = File::load($item['target_id']);
                  if (!empty($file)) {
                    $item = \Drupal::service('file_url_generator')->generateAbsoluteString($file->get('uri')->value);
                  }
                }
                else {

                  // Exception for references entities.
                  if (($type === 'entity_reference' || $type === 'entity_reference_revisions') && is_numeric(
                      $item['target_id']
                    )
                  ) {

                    $origItem = $item;

                    // Ensure the entity has the fieldkey (required or issues
                    // exist on paragraphs)
                    if ($entity->hasField($fieldKey)) {
                      $referencedEntities = $entity->get($fieldKey)
                        ->referencedEntities();
                      if (!empty($referencedEntities)) {
                        $targetEntityType = $referencedEntities[$fieldItemKey]->getEntityTypeId();
                        $subEntity = $this->load($targetEntityType, $item['target_id']);

                        if ($targetEntityType === 'taxonomy_term') {
                          // Exception for taxonomy terms, show the machine name
                          // or TID if machine name does not exist
                          // Check if the machine name module has been used.
                          if ($subEntity->hasField('machine_name')) {
                            $machineName = $subEntity->get('machine_name')->value;
                            $item = $machineName;
                          }
                        }
                        else {
                          // Exception for all other entities that can be
                          // referenced (paragraphs, users, ...)
                          $item = $this->convertEntityToArray($subEntity, $external_use, [], $uuids);
                        }
                      }
                    }

                    // If none of the above conditions worked, fallback to the
                    // target_id.
                    if ($item === $origItem) {
                      $item = $item['target_id'];
                    }

                  }
                  else {
                    $item = $item['target_id'];
                  }
                }
              }
            }
          }

          // Remove the levels for the fields that only contain a single value
          // This should only be applied when the field allows only 1 input,
          // this way a multiple input field will always display as an array
          // If not, this would convert a single input in a multiple input field
          // to an output of a single field, making it
          // impossible to know that the field can contain multiple values in
          // certain instances.
          if ($cardinality === 1 && is_array($entityArray[$fieldKey]) && count($entityArray[$fieldKey]) === 1) {
            $entityArray[$fieldKey] = $entityArray[$fieldKey][0];
          }
          else {
            if (empty($entityArray[$fieldKey])) {
              $entityArray[$fieldKey] = NULL;
            }
          }
        }
      }
      else {
        $entityArray[$fieldKey] = NULL;
      }
    }

    // Place the content inside the UUIDS for the infinite loop fix
    // Return the value.
    return $uuids[$uuid] = $entityArray;
  }

}
