<?php

namespace Drupal\general\Service;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Mail\MailManager;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\general\Data\BatchGeneralData;
use Drupal\general\Manager\DataManager;

/**
 * Processor helper service.
 */
class ProcessorHelper {

  /**
   * ReconciliationHelper constructor.
   *
   * @param \Drupal\Core\File\FileSystem $fileSystem
   *   The file system service.
   * @param \Drupal\Core\Mail\MailManager $mailManager
   *   The mail manager service.
   * @param \Drupal\general\Manager\DataManager $dataManager
   *   The data manager.
   */
  public function __construct(protected FileSystem $fileSystem, protected MailManager $mailManager, protected DataManager $dataManager) {}

  /**
   * Make a file.
   *
   * @param array $item
   *   The data to create a file with.
   * @param \Drupal\general\Data\BatchGeneralData $generalData
   *   The general data for this batch.
   *
   * @return array
   *   Data for the created file.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function makeFile(array $item, BatchGeneralData $generalData): array {
    $newFileName = $generalData->getEmail() . '_' . $generalData->getTime() . '_' . $item['step'] . '.csv';
    $uri = 'public://queue/processed_' . $item['step'] . '/' . $newFileName;

    // Create new CSV file if not already exists.
    $fileEntity = File::create([
      'uid' => 1,
      'filename' => $newFileName,
      'uri' => $uri,
      'status' => 1,
    ]);
    $fileEntity->setPermanent();
    $fileEntity->save();

    // Create the directory for the file on the disk.
    $dir = dirname($fileEntity->getFileUri());
    if (!file_exists($dir)) {
      mkdir($dir, 0770, TRUE);
    }

    // The original row.
    return [
      'uri' => $uri,
      'entity' => $fileEntity,
    ];
  }

  /**
   * Add a row to a file.
   *
   * @param string $uri
   *   The file URI.
   * @param array $row
   *   The row data.
   * @param string|int $step
   *   The current step.
   * @param \Drupal\general\Data\BatchGeneralData $generalData
   *   The general data for this batch.
   *
   * @return string|false
   *   The absolute local filepath (with no symbolic links) or FALSE on failure.
   */
  public function addRowToFile(string $uri, array $row, string|int $step, BatchGeneralData $generalData) {
    // Check if its a new file to add the headers.
    $realpath = $this->fileSystem->realpath($uri);
    $isNew = !file_exists($realpath);

    // Re-order rows as the is one added (wikidatalink)
    // 'a' for append to file - created if doesn't exit.
    $file = fopen($realpath, 'a');
    if ($isNew) {
      $headers = [
        'Identifier',
        'Objectnaam',
        'Creatiedatum',
        'Creatieplaats',
        'Vervaardiger',
        'Geboortedatum',
        'Sterfdatum',
        'Wikidata',
        'Wikidata Link',
        'VIAF',
        'RKD',
        'ULAN',
        'Type vervaardiger',
        'Auteursrechtenstatus',
      ];
      if ((int) $step === 2) {
        $headers[] = 'Jaar waarop dit publiek wordt';
        $headers[] = 'Auteursrechten Vertegenwoordiger';
      }
      $headers[] = 'Foutcode';
      $headers[] = 'Foutmelding';
      fputcsv($file, $headers);
    }
    fputcsv($file, $row);
    fclose($file);

    $this->updateProcessed($generalData->getCsvId());
    return $realpath;
  }

  /**
   * Update processed.
   *
   * @param string|int $csv_id
   *   The CSV ID.
   */
  protected function updateProcessed(string|int $csv_id) {
    $csv = $this->dataManager->getCsv($csv_id)->getEntity();
    $processed = (int) $csv->get('field_processed')->value + 1;
    $total = (int) $csv->get('field_total')->value;
    $csv->set('field_processed', $processed);
    $csv->set('field_processed_total', round($processed / $total, 2) * 100);
    $csv->save();
  }

  /**
   * Set completed.
   *
   * @param string $csv_id
   *   The CSV ID.
   */
  protected function setCompleted(string $csv_id) {
    $csv = $this->dataManager->getCsv($csv_id)->getEntity();
    $csv->set('field_completed', 1);
    $csv->save();
  }

  /**
   * Send a mail on complete.
   *
   * @param array $item
   *   The item data.
   * @param string $realpath
   *   The real path.
   * @param \Drupal\Core\Entity\EntityInterface $file_entity
   *   The file entity.
   * @param \Drupal\general\Data\BatchGeneralData $generalData
   *   The general data for this batch.
   */
  public function sendMailOnComplete(array $item, string $realpath, EntityInterface $file_entity, BatchGeneralData $generalData) {
    // Get the number of rows in the new CSV file.
    $numberOfRows = 0;
    $file = fopen($realpath, 'r');
    if ($file) {
      while (!feof($file)) {
        $content = fgets($file);
        if ($content) {
          $numberOfRows++;
        }
      }
    }
    fclose($file);

    // If number of rows in new CSV = number of rows in old file
    // ($generalData->getNumberOfRows) notify the user.
    if ($numberOfRows === (int) $generalData->getNumberOfRows() + 1) {
      $this->setCompleted($generalData->getCsvId());

      $to = $generalData->getEmail();
      $emailNode = $this->dataManager->getEmailContent('process_step_' . $item['step'] . '_finished')->getEntity();
      $body = $emailNode->get('body')->value;

      // Replace tokens in the mail body.
      // Filelink.
      if (str_contains($body, '[file-link]')) {
        $fileLink = $file_entity->createFileUrl(FALSE);
        $fileLink = '<a href="' . $fileLink . '">' . $fileLink . '</a>';
        $body = str_replace('[file-link]', $fileLink, $body);
      }

      // Link to step 2.
      if (strpos($body, '[step-2-link]') !== FALSE) {
        $backlink = Url::fromRoute('entity.node.canonical', [
          'node' => 8,
          'did' => $generalData->getCsvId(),
          'valid' => 'valid',
        ], ['absolute' => TRUE])->toString();
        $backlink = '<a href="' . $backlink . '">' . $backlink . '</a>';
        $body = str_replace('[step-2-link]', $backlink, $body);
      }

      $params['message'] = Markup::create($body);
      $params['subject'] = $emailNode->label();
      $this->mailManager->mail('general', 'process_finished', $to, 'nl', $params);
    }
  }

}
