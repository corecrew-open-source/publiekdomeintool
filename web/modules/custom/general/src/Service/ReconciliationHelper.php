<?php

namespace Drupal\general\Service;

use Drupal\Core\Transliteration\PhpTransliteration;
use Drupal\general\CsvRowEnum;

/**
 * Helper service for reconciliation.
 */
class ReconciliationHelper {

  /**
   * ReconciliationHelper constructor.
   */
  public function __construct(protected SSLHelper $sslHelper, protected PhpTransliteration $transliteration) {}

  /**
   * Get reconciliation data.
   *
   * @param mixed $row
   *   Row to get reconciliation data for.
   *
   * @return mixed|bool
   *   The reconciliation data.
   */
  public function getReconData($row) {
    // Build the params to send to the reconciliation API.
    // Filter out characters that may cause errors.
    $row[CsvRowEnum::creator()->value] = $this->transliteration->transliterate($row[CsvRowEnum::creator()->value]);
    $dataToSend = [
      'data' => [
        'query' => utf8_encode($row[CsvRowEnum::creator()->value]),
        'type' => 'Q5',
        'type_strict' => 'should',
      ],
    ];

    $properties = [];
    if ($row[CsvRowEnum::date_of_birth()->value] !== '') {
      $properties[] = [
        'pid' => 'P569',
        'v' => $row[CsvRowEnum::date_of_birth()->value],
      ];
    }
    if ($row[CsvRowEnum::date_of_death()->value] !== '') {
      $properties[] = [
        'pid' => 'P570',
        'v' => $row[CsvRowEnum::date_of_death()->value],
      ];
    }

    // If the birth and death dates are set in the input file.
    if (count($properties) > 0) {
      $dataToSend['data']['properties'] = $properties;
    }

    $baseUrl = 'https://wikidata.reconci.link/en/api?queries=';
    $url = $baseUrl . urlencode(json_encode($dataToSend));
    $openRefineData = $this->sslHelper->getSslData($url);
    $openRefineData = json_decode($openRefineData);
    if (isset($openRefineData->data)) {
      $openRefineData = $openRefineData->data;
      return $openRefineData->result;
    }

    // Catch empty responses.
    $data['status'] = FALSE;
    $data['error_code'] = is_numeric(substr($openRefineData->details, 0, 3))
      ? substr($openRefineData->details, 0, 3)
      : '500';
    $data['message'] = $openRefineData->message . ' - ' . $openRefineData->details;

    return $data;
  }

}
