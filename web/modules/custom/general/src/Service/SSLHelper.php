<?php

namespace Drupal\general\Service;

/**
 * SSL Helper service.
 */
class SSLHelper {

  /**
   * Get SSL data for a given url.
   *
   * @param string $url
   *   The URL to get SSL data for.
   *
   * @return mixed
   *   The open refine data.
   */
  public function getSslData(string $url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_REFERER, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $openRefineData = curl_exec($ch);
    curl_close($ch);
    return $openRefineData;
  }

}
