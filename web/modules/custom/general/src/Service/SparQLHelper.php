<?php

namespace Drupal\general\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\general\Api\WikiDataEndpoint;

/**
 * Helper service for SparQL.
 */
class SparQLHelper {

  /**
   * SparQLHelper constructor.
   */
  public function __construct(protected EntityTypeManagerInterface $entityTypeManager) {}

  /**
   * Perform a SparQL query.
   *
   * @param string $sparqlQuery
   *   The SparQL query to perform.
   *
   * @return mixed
   *   SparQL query data.
   */
  private function query(string $sparqlQuery) {
    $opts = [
      'http' => [
        'method' => 'GET',
        'header' => [
          'Accept: application/sparql-results+json',
    // @todo adjust this; see https://w.wiki/CX6
          'User-Agent: WDQS-example PHP/' . PHP_VERSION,
        ],
      ],
    ];
    $context = stream_context_create($opts);

    $url = 'https://query.wikidata.org/sparql?query=' . urlencode($sparqlQuery);
    $response = file_get_contents($url, FALSE, $context);
    return json_decode($response, TRUE);
  }

  /**
   * Get all athlete categories.
   */
  public function getAllAthleteCategories() {
    return $this->getAllSubCategoriesQuery('Q2066131');
  }

  /**
   * Get all politician categories.
   */
  public function getAllPoliticianCategories() {
    return $this->getAllSubCategoriesQuery('Q82955');
  }

  /**
   * Get all artist categories.
   */
  public function getAllArtistCategories() {
    return $this->getAllSubCategoriesQuery('Q483501');
  }

  /**
   * Get all categories.
   */
  public function getAllCategories() {
    $tree = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree(
    // This is your taxonomy term vocabulary (machine name).
      'categories',
    // This is "tid" of parent. Set "0" to get all.
      0,
    // Get only 1st level.
      1,
    // Get full load of taxonomy term entity.
      TRUE
    );

    $results = [];
    foreach ($tree as $term) {
      $qid = $term->get('field_qid')->value;
      if ($qid !== 'NONE') {
        $results[$qid] = $this->getAllSubCategoriesQuery($qid);
        ;
      }
    }

    return $results;
  }

  /**
   * Get query for all subcategories.
   */
  private function getAllSubCategoriesQuery($qid) {
    return $this->query('SELECT ?id WHERE { ?id (wdt:' . (new WikiDataEndpoint())->getProperty('subclass of') . '*) wd:' . $qid . '. }');
  }

}
