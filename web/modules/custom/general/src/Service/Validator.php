<?php

namespace Drupal\general\Service;

/**
 * Validator service.
 */
class Validator {

  const NUMBER_OF_COLUMNS_STEP_1 = 11;
  const NUMBER_OF_COLUMNS_STEP_2 = 16;
  const MIN_NUMBER_OF_COLUMNS_STEP_2 = 14;
  // When no errors occur during Step 1, there will be fewer columns.
  // Use the minimum value as a lower limit for validation.

  /**
   * Validates number of columns.
   *
   * @param array $row
   *   The row data.
   * @param string|int $step
   *   The step.
   *
   * @return bool
   *   Whether number of columns validates or not.
   */
  public function validateNumberOfColumns(array $row, string|int $step = 1): bool {
    if (count($row) === self::NUMBER_OF_COLUMNS_STEP_1 && (int) $step === 1) {
      return TRUE;
    }
    // Columns count for Step 2 should be either 14 or 16 when there were server errors.
    if ((count($row) == self::MIN_NUMBER_OF_COLUMNS_STEP_2 || count($row) == self::NUMBER_OF_COLUMNS_STEP_2) && (int) $step === 2) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Validates date.
   *
   * @param string $date
   *   Date to validate.
   * @param bool $validate
   *   Whether to validate.
   *
   * @return mixed|bool
   *   Whether date validates, or visual value.
   */
  public function validateDate(string $date, bool $validate = TRUE) {
    // For testing go to https://www.regextester.com/
    $patternArr = [
      'yyyy-mm-dd' => '/^([012]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$/i',
      'yyyy-mm' => '/^([012]\d{3}-(0[1-9]|1[0-2]))$/',
      'yyyy' => '/^([012]\d{3})$/',
      'circa yyyy' => '/^circa ([012]\d{3})$/',
      'ca yyyy' => '/^ca ([012]\d{3})$/',
      'ca. yyyy' => '/^ca\. ([012]\d{3})$/',
    // 'yyyy-yyyy' => '/^([012]\d{3})-([012]\d{3})$/',
    //      'yyyy - yyyy' => '/^([012]\d{3}) - ([012]\d{3})/',
    //      'circa yyyy - circa yyyy' => '/^circa ([012]\d{3}) - circa ([012]\d{3})$/',
    //      'ca. yyyy - ca. yyyy' => '/^ca\. ([012]\d{3}) - ca\. ([012]\d{3})$/',
    //      'ca yyyy - ca yyyy' => '/^ca ([012]\d{3}) - ca ([012]\d{3})$/',
    ];

    foreach ($patternArr as $visual => $pattern) {
      if (preg_match($pattern, $date) === 1) {
        if ($validate === TRUE) {
          return TRUE;
        }
        else {
          return $visual;
        }
      }
    }
    return FALSE;
  }

  /**
   * Converts a date to year.
   *
   * @param string $date
   *   Date to convert.
   *
   * @return string|int
   *   Converted value.
   */
  public function convertDateToYear(string $date): int|string {
    $visual = $this->validateDate($date, FALSE);
    $date = $this->removeCirca($date);

    $normal = [
      'yyyy',
      'circa yyyy',
      'ca yyyy',
      'ca. yyyy',
    ];
    if (in_array($visual, $normal)) {
      return (int) $date;
    }

    if ($visual === 'yyyy-mm-dd') {
      return (new \DateTime($date))->format('Y');
    }

    return (new \DateTime($date . '-01'))->format('Y');
  }

  /**
   * Remove circa values, and make date more specific.
   *
   * @param string $date
   *   The date value.
   *
   * @return string
   *   The date with circa removed.
   */
  public function removeCirca(string $date): string {
    $date = str_replace(['circa', 'ca.', 'ca'], '', $date);
    return trim($date);
  }

  /**
   * Validates QID.
   *
   * @param string $qid
   *   The QID to validate.
   *
   * @return bool
   *   Whether QID validates.
   */
  public function validateQid(string $qid): bool {
    $regex = '/^Q\d{0,99}$/';
    if (preg_match($regex, $qid) === 1) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Validates number.
   *
   * @param string $number
   *   The number to validate.
   *
   * @return bool
   *   Whether number validates.
   */
  public function validateNumber(string $number): bool {
    $regex = '/^\d{0,99}$/';
    if (preg_match($regex, $number) === 1) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get delimiter.
   *
   * @param mixed $fh
   *   The file handler.
   *
   * @return string
   *   The delimiter.
   *
   * @see https://stackoverflow.com/questions/26717462/php-best-approach-to-detect-csv-delimiter
   */
  public function getDelimiter($fh): string {
    $delimiters = ["\t", ";", "|", ","];
    $data1 = [];
    $data2 = [];
    $delimiter = $delimiters[0];
    foreach ($delimiters as $d) {
      $data1 = fgetcsv($fh, 4096, $d);
      if (@count($data1) > @count($data2)) {
        $delimiter = $d;
        $data2 = $data1;
      }
      rewind($fh);
    }

    return $delimiter;
  }

}
