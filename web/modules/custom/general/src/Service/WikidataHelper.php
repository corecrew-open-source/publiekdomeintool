<?php

namespace Drupal\general\Service;

use DataValues\Deserializers\DataValueDeserializer;
use DataValues\MonolingualTextValue;
use DataValues\Serializers\DataValueSerializer;
use DataValues\StringValue;
use DataValues\TimeValue;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\general\Api\WikiDataEndpoint;
use Drupal\general\CsvErrorRowEnum;
use Drupal\general\CsvRowEnum;
use Drupal\general\Data\BatchGeneralData;
use Drupal\general\Entity\WikidataUpdate;
use Drupal\taxonomy\TermInterface;
use Mediawiki\Api\ApiUser;
use Mediawiki\Api\MediawikiApi;
use Mediawiki\Api\UsageException;
use Mediawiki\DataModel\Revision;
use Wikibase\Api\Service\RevisionGetter;
use Wikibase\Api\WikibaseFactory;
use Wikibase\DataModel\Entity\EntityIdValue;
use Wikibase\DataModel\Entity\Item;
use Wikibase\DataModel\Entity\ItemId;
use Wikibase\DataModel\Entity\PropertyId;
use Wikibase\DataModel\ItemContent;
use Wikibase\DataModel\Snak\PropertyValueSnak;
use Wikibase\DataModel\Statement\Statement;
use Wikibase\DataModel\Statement\StatementList;

/**
 * Wikidata helper service.
 */
class WikidataHelper {

  use StringTranslationTrait;

  const WIKIDATA_DATEFORMAT = '+Y-m-d\T00:00:00\Z';

  /**
   * The general logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The Wikidata config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $wikiDataConfig;

  /**
   * The current WikiBase factory.
   *
   * @var \Wikibase\Api\WikibaseFactory|null
   */
  protected ?WikibaseFactory $wbFactory;

  /**
   * The current getter object.
   *
   * @var \Wikibase\Api\Service\RevisionGetter|null
   */
  protected ?RevisionGetter $getter = NULL;

  /**
   * The current revision object.
   *
   * @var \Mediawiki\DataModel\Revision|null
   */
  protected ?Revision $revision = NULL;

  /**
   * WikidataHelper class constructor.
   *
   * @param \Drupal\general\Service\Validator $validator
   *   The validator service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   The logger channel factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(
    protected Validator $validator,
    LoggerChannelFactory $logger,
    ConfigFactoryInterface $config_factory) {
    $this->logger = $logger->get('general');
    $this->wikiDataConfig = $config_factory->get('general.wikidata.settings');
  }

  /**
   * Get Wikidata API.
   *
   * @return \Wikibase\Api\WikibaseFactory
   *   The Wikidata base factory.
   *
   * @throws \Mediawiki\Api\UsageException
   */
  protected function getApi() {
    $api_env = $this->wikiDataConfig->get('wikidata_api_environment');
    $wd_api = new WikiDataEndpoint($api_env);
    $api = new MediawikiApi($wd_api->getEndpointUrl());
    $api->login(new ApiUser($this->wikiDataConfig->get('wikidata_api_credentials.user'), $this->wikiDataConfig->get('wikidata_api_credentials.pass')));

    // Create our Factory, All services should be used through this!
    // You will need to add more or different datavalues here.
    // In the future Wikidata / Wikibase defaults will be provided in a separate
    // library.
    $dataValueClasses = [
      'unknown' => 'DataValues\UnknownValue',
      'string' => 'DataValues\StringValue',
      'boolean' => 'DataValues\BooleanValue',
      'number' => 'DataValues\NumberValue',
      'globecoordinate' => 'DataValues\Geo\Values\GlobeCoordinateValue',
      'monolingualtext' => 'DataValues\MonolingualTextValue',
      'multilingualtext' => 'DataValues\MultilingualTextValue',
      'quantity' => 'DataValues\QuantityValue',
      'time' => 'DataValues\TimeValue',
      'wikibase-entityid' => 'Wikibase\DataModel\Entity\EntityIdValue',
    ];
    return new WikibaseFactory(
      $api,
      new DataValueDeserializer($dataValueClasses),
      new DataValueSerializer()
    );
  }

  /**
   * Prepare a new revision for WikiData.
   *
   * @param string|\Wikibase\DataModel\Entity\EntityId $qid
   *   The qid.
   */
  public function prepareNewRevision($qid) {
    $this->wbFactory = $this->getApi();
    $this->getter = $this->wbFactory->newRevisionGetter();
    $this->revision = $this->getter->getFromId($qid);
  }

  /**
   * Save a new revision to WikiData.
   */
  public function saveNewRevision() {
    $saver = $this->wbFactory->newRevisionSaver();
    $saver->save($this->revision);
  }

  /**
   * Create statement for Wikidata.
   *
   * @param string|\Wikibase\DataModel\Entity\EntityId $qid
   *   The qid.
   * @param string $propertyId
   *   The property ID.
   * @param string $value
   *   The value to send.
   * @param array $row
   *   The row.
   * @param \Drupal\general\Data\BatchGeneralData $generalData
   *   The general data for this batch.
   *
   * @return bool
   *   Whether save is needed.
   *
   * @throws \Mediawiki\Api\UsageException
   * @throws \Exception
   */
  public function createStatement($qid, string $propertyId, string $value, array &$row, BatchGeneralData $generalData): bool {
    // Get the global context variables for local use.
    $getter = $this->getter;

    if (str_starts_with($propertyId, 'P')) {
      $propertyId = (int) str_replace('P', '', $propertyId);
    }

    $saveNeeded = FALSE;
    $dateSave = 0;
    $item = $this->revision->getContent()->getData();
    /** @var \Wikibase\DataModel\Statement\StatementList $statementList */
    $statementList = $item->getStatements();
    $property = $statementList->getByPropertyId(PropertyId::newFromNumber($propertyId));

    if ($property->isEmpty()) {
      $saveNeeded = TRUE;
    }
    else {
      $wikidataValues = [];
      foreach ($property->getIterator() as $statement) {
        $snak = $statement->getMainSnak();
        if ($snak->getDataValue() instanceof EntityIdValue) {
          $wikidataValue = 'Q' . $snak->getDataValue()->getEntityId()->getNumericId();
        }
        elseif ($snak->getDataValue() instanceof TimeValue) {
          $wikidataValue = $snak->getDataValue()->getTime();
          $wikidataPrecision = $snak->getDataValue()->getPrecision();
          $pattern = $this->validator->validateDate($value, FALSE);
          if ($pattern === 'yyyy-mm-dd') {
            $dateTimePattern = 'Y-m-d';
            $convertedValue = $value;
            $valuePrecision = TimeValue::PRECISION_DAY;
          }
          elseif ($pattern === 'yyyy-mm') {
            $dateTimePattern = 'Y-m';
            $convertedValue = $value;
            $valuePrecision = TimeValue::PRECISION_MONTH;
          }
          else {
            $dateTimePattern = 'Y';
            $convertedValue = $this->validator->removeCirca($value);
            $valuePrecision = TimeValue::PRECISION_YEAR;
          }

          if ($wikidataPrecision === TimeValue::PRECISION_YEAR) {
            $wikidataFormat = 'Y';
            $length = 4;
          }
          elseif ($wikidataPrecision === TimeValue::PRECISION_MONTH) {
            $wikidataFormat = 'Y-m';
            $length = 7;
          }
          elseif ($wikidataPrecision === TimeValue::PRECISION_DAY) {
            $wikidataFormat = 'Y-m-d';
            $length = 10;
          }

          $valueDatetime = \DateTime::createFromFormat($dateTimePattern, $convertedValue);
          $wikidataDatetime = \DateTime::createFromFormat($wikidataFormat, substr($wikidataValue, 1, $length));

          // Compare values if dates have the same precision.
          if ($valuePrecision === $wikidataPrecision) {
            // Check if value is different. If not write, same -> add refernce.
            if ($valueDatetime->format($dateTimePattern) !== $wikidataDatetime->format($dateTimePattern)) {
              $dateSave++;
            }
            elseif ($statement->getReferences()->isEmpty()) {
              // Add the reference if the statement is the same and has no
              // references.
              $this->addReferenceToStatement($statement, $row, $getter, $generalData, $propertyId, $value);
            }
            else {
              // Check if the reference is not already in wikidata.
              if (!$this->hasIdenticalReference($statement, $row, $generalData->getOrganisationQid(), $value)) {
                $this->addReferenceToStatement($statement, $row, $getter, $generalData, $propertyId, $value);
              }
            }
          }

          // Write date if new precision is more precise.
          if ($valuePrecision > $wikidataPrecision) {
            $dateSave++;
          }

          // If value in wikidata is more precise check if value is the same.
          if ($valuePrecision < $wikidataPrecision) {
            // If value is not the same write the date.
            if ($valueDatetime->format($dateTimePattern) !== $wikidataDatetime->format($dateTimePattern)) {
              $dateSave++;
            }
          }
        }
        elseif ($snak->getDataValue() instanceof StringValue) {
          $wikidataValue = $snak->getDataValue()->getValue();
        }

        if (isset($wikidataValue)) {
          $wikidataValues[] = $wikidataValue;
        }
      }
    }

    // This won't work for dates as this doesn't take into account the
    // precision.
    if ((isset($wikidataValues) && !in_array($value, $wikidataValues) && !$this->isDate($propertyId))
      // Take into account precision of the dates.
      || ($dateSave === count($property->getIterator()) && $this->isDate($propertyId))) {
      $saveNeeded = TRUE;
    }

    // Add reference if exact value is in wikidata.
    if (isset($wikidataValues) && in_array($value, $wikidataValues) && !$this->isDate($propertyId)) {
      $statementInt = array_search($value, $wikidataValues);
      $existingStatement = $property->getIterator()[$statementInt];
      if ($existingStatement->getReferences()->isEmpty()) {
        $this->addReferenceToStatement($existingStatement, $row, $getter, $generalData, $propertyId, $value);
      }
      else {
        // Check if the reference is not already in wikidata.
        if (!$this->hasIdenticalReference($existingStatement, $row, $generalData->getOrganisationQid(), $value)) {
          $this->addReferenceToStatement($existingStatement, $row, $getter, $generalData, $propertyId, $value);
        }
      }
    }

    // Create new statement and add reference to it.
    if ($saveNeeded) {
      $this->addStatement($statementList, $row, $qid, $propertyId, $value, $getter, $generalData);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Add statement for Wikidata.
   *
   * @param \Wikibase\DataModel\Statement\StatementList $statementList
   *   The statement list to add created statement to.
   * @param array $row
   *   The data row.
   * @param string|\Wikibase\DataModel\Entity\EntityId $qid
   *   The qid.
   * @param int|string $propertyId
   *   The property ID.
   * @param string $value
   *   The value to send.
   * @param \Wikibase\Api\Service\RevisionGetter $getter
   *   A revision getter.
   * @param \Drupal\general\Data\BatchGeneralData $generalData
   *   The general data for this batch.
   *
   * @throws \Exception
   */
  protected function addStatement(StatementList $statementList, array &$row, $qid, int|string $propertyId, string $value, RevisionGetter $getter, BatchGeneralData $generalData) {
    $wd_endpoint = new WikiDataEndpoint();
    $newValue = new StringValue($value);
    if (str_starts_with($value, 'Q')) {
      if ($wikidata_item = $getter->getFromId($value)) {
        $newValue = new EntityIdValue($wikidata_item->getContent()->getData()->getId());
      }
      else {
        // Add an error message for this row.
        $row[CsvErrorRowEnum::step2_error_code()->value] = 404;
        $row[CsvErrorRowEnum::step2_error_message()->value] = $this->t('Het Wikidata item met ID @id kon niet gevonden worden', ['@id' => $value]);
        return;
      }
    }

    if ($this->isDate($propertyId)) {
      $pattern = $this->validator->validateDate($value, FALSE);
      if ($pattern === 'yyyy-mm-dd') {
        $newValue = $this->dateConverter($value . 'T00:00:00+02:00');
      }
      elseif ($pattern === 'yyyy-mm') {
        $newValue = $this->dateConverter($value . '-01T00:00:00+02:00', TimeValue::PRECISION_MONTH);
      }
      else {
        $value = $this->validator->removeCirca($value);
        $newValue = $this->dateConverter($value . '-01-01T00:00:00+02:00', TimeValue::PRECISION_YEAR);
      }

      // Only save year to property floruit.
      if ($newValue && $propertyId === $wd_endpoint->getProperty('floruit')) {
        $createdDate = (new \DateTime($newValue->getTime()))->format('Y');
        $newValue = $this->dateConverter($createdDate . '-01-01T00:00:00+02:00', TimeValue::PRECISION_YEAR);
      }
    }

    $this->updateWikidataUpdate($propertyId, $qid, $generalData->getEmail(), $generalData->getOrganisationQid(), 'statement', $value);

    $property_value_snak = new PropertyValueSnak(
      PropertyId::newFromNumber($propertyId),
      $newValue
    );
    $statement = new Statement($property_value_snak);
    $reference_added = $this->addReferenceToStatement($statement, $row, $getter, $generalData, $propertyId, $value);
    // Only add a statement if there is a valid accompanying reference.
    if ($reference_added) {
      $statementList->addStatement($statement);
      $this->revision->getContent()->getData()->setStatements($statementList);
    }
  }

  /**
   * Add a reference to a statement object.
   *
   * @param \Wikibase\DataModel\Statement\Statement $statement
   *   The statement object.
   * @param array $row
   *   The row.
   * @param \Wikibase\Api\Service\RevisionGetter $getter
   *   The revision getter.
   * @param \Drupal\general\Data\BatchGeneralData $generalData
   *   The general data for this batch.
   * @param int|string $propertyId
   *   The property ID.
   * @param string $value
   *   The value.
   *
   * @return bool
   *   Whether reference was correctly added to statement.
   */
  protected function addReferenceToStatement(Statement $statement, array $row, RevisionGetter $getter, BatchGeneralData $generalData, int|string $propertyId, string $value): bool {
    $wd_endpoint = new WikiDataEndpoint();
    $referenceSnaks = [
      new PropertyValueSnak(new PropertyId($wd_endpoint->getProperty('retrieved')), $this->dateConverter()),
    ];

    if (filter_var($row[CsvRowEnum::identifier()->value], FILTER_VALIDATE_URL)) {
      $referenceSnaks[] = new PropertyValueSnak(new PropertyId($wd_endpoint->getProperty('reference url')), new StringValue($row[CsvRowEnum::identifier()->value]));
    }
    else {
      // If there is no reference URL, create one using the properties below.
      // If not all properties are present, do not write to Wikidata.
      if (!$generalData->getDatasetQid() || !$generalData->getOrganisationQid()) {
        return FALSE;
      }

      $dataset_entity = $this->qidEntityConvert($generalData->getDatasetQid(), $getter);
      $organisation_entity = $this->qidEntityConvert($generalData->getOrganisationQid(), $getter);
      if (!$dataset_entity || !$organisation_entity) {
        return FALSE;
      }
      $referenceSnaks[] = new PropertyValueSnak(new PropertyId($wd_endpoint->getProperty('catalog')), $dataset_entity);
      $referenceSnaks[] = new PropertyValueSnak(new PropertyId($wd_endpoint->getProperty('collection')), $organisation_entity);
      $referenceSnaks[] = new PropertyValueSnak(new PropertyId($wd_endpoint->getProperty('catalog code')), new StringValue($row[CsvRowEnum::identifier()->value]));
    }

    if ($this->isDate($propertyId)) {
      $referenceSnaks[] = new PropertyValueSnak(new PropertyId($wd_endpoint->getProperty('quotation')), new MonolingualTextValue('nl', $value));
    }

    $statement->addNewReference($referenceSnaks);
    return TRUE;
  }

  /**
   * Updata wikidata entry.
   *
   * @param int|string $propertyId
   *   The property ID.
   * @param string|\Wikibase\DataModel\Entity\EntityId|\Wikibase\DataModel\Entity\ItemId $qid
   *   The qid.
   * @param string $email
   *   The email address.
   * @param int|string $userQid
   *   The user qid.
   * @param string $type
   *   The type.
   * @param string $value
   *   The value to send.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function updateWikidataUpdate(int|string $propertyId, $qid, string $email, int|string $userQid, string $type, string $value): void {
    $wikidataUpdate = WikidataUpdate::create([
      'status' => 1,
      'field_property' => $propertyId,
      'field_qid' => $qid,
      'field_email' => $email,
      'field_qid_user' => $userQid,
      'field_status' => $type,
      'field_value' => $value,
    ]);
    $wikidataUpdate->save();
  }

  /**
   * Helper for Qid entity conversion.
   *
   * @param string $qid
   *   The qid.
   * @param \Wikibase\Api\Service\RevisionGetter $getter
   *   The statement getter.
   *
   * @return \Wikibase\DataModel\Entity\EntityIdValue|null
   *   The EntityIdValue.
   */
  protected function qidEntityConvert(string $qid, RevisionGetter $getter): ?EntityIdValue {
    if ($item = $getter->getFromId($qid)) {
      return new EntityIdValue($item->getContent()->getData()->getId());
    }
    return NULL;
  }

  /**
   * Helper to convert date to TimeValue.
   *
   * @param string $date
   *   The date to convert.
   * @param int $precision
   *   The precision.
   *
   * @return \DataValues\TimeValue
   *   The converted TimeValue.
   *
   * @throws \Exception
   */
  protected function dateConverter(string $date = 'now', int $precision = TimeValue::PRECISION_DAY): TimeValue {
    $createdDate = (new \DateTime($date))->format(self::WIKIDATA_DATEFORMAT);
    return new TimeValue($createdDate, 60, 0, 0, $precision, TimeValue::CALENDAR_GREGORIAN);
  }

  /**
   * Checks whether property is date.
   *
   * @param int|string $propertyId
   *   The property ID.
   *
   * @return bool
   *   Whether it is a date.
   */
  protected function isDate(int|string $propertyId): bool {
    $wd_endpoint = new WikiDataEndpoint();
    return in_array('P' . $propertyId, $wd_endpoint->getDateProperties());
  }

  /**
   * Creates a new item on WikiData.
   *
   * @param array $row
   *   The data row.
   * @param \Drupal\general\Data\BatchGeneralData $generalData
   *   The general data for this batch.
   * @param \Drupal\taxonomy\TermInterface|null $occupation_term
   *   The occupation term.
   *
   * @return \Wikibase\DataModel\Entity\ItemId|null
   *   The ItemId.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Mediawiki\Api\UsageException
   */
  public function createNewItem(array &$row, BatchGeneralData $generalData, ?TermInterface $occupation_term = NULL): ?ItemId {
    $this->wbFactory = $this->getApi();
    $saver = $this->wbFactory->newRevisionSaver();

    // Format Wikidata item label.
    $label = $row[CsvRowEnum::creator()->value];
    // Format Wikidata item description.
    $description_en = $description_nl = $description_fr = '';
    // Get occupation label from WikiData in all relevant languages.
    if ($occupation_term) {
      $occupation_wikidata_qid = $occupation_term->get('field_qid')->getValue()[0]['value'];
      if ($occupation_wikidata_qid !== 'NONE') {
        $description_en = $this->getItemLabel($occupation_wikidata_qid, 'en');
        $description_nl = $this->getItemLabel($occupation_wikidata_qid, 'nl');
        $description_fr = $this->getItemLabel($occupation_wikidata_qid, 'fr');
      }
    }
    elseif (strtolower($row[CsvRowEnum::instance_of()->value]) === 'organisatie') {
      $description_nl = 'organisatie';
      $description_en = 'organisation';
      $description_fr = 'organisation';
    }
    // Add date of birth / death data to description if available.
    if (!empty($row[CsvRowEnum::date_of_birth()->value]) || !empty($row[CsvRowEnum::date_of_death()->value])) {
      $dob_year = $dod_year = '';
      if (!empty($row[CsvRowEnum::date_of_birth()->value])) {
        $dob_year = $this->validator->convertDateToYear($row[CsvRowEnum::date_of_birth()->value]);
      }
      if (!empty($row[CsvRowEnum::date_of_death()->value])) {
        $dod_year = $this->validator->convertDateToYear($row[CsvRowEnum::date_of_death()->value]);
      }
      $date_suffix = " ({$dob_year} - {$dod_year})";
      $description_en = $description_en ? $description_en . $date_suffix : $description_en;
      $description_nl = $description_nl ? $description_nl . $date_suffix : $description_nl;
      $description_fr = $description_fr ? $description_fr . $date_suffix : $description_fr;
    }
    $valid_description = !empty($description_en) || !empty($description_fr) || !empty($description_nl);

    if ($this->validDataForItemCreation($row, $valid_description, $generalData, $occupation_term)) {
      // Create new item to send to WikiData, with relevant labels &
      // descriptions filled in.
      $new_item = new Item();
      $new_item->setLabel('en', $label);
      $new_item->setLabel('nl', $label);
      $new_item->setLabel('fr', $label);
      $new_item->setDescription('en', $description_en);
      $new_item->setDescription('nl', $description_nl);
      $new_item->setDescription('fr', $description_fr);
      $edit = new Revision(
        new ItemContent($new_item)
      );
      try {
        $resultingItem = $saver->save($edit);
        // Get the ItemId object of the created item.
        $itemId = $resultingItem->getId();
        $this->updateWikidataUpdate(0, $itemId, $generalData->getEmail(), $generalData->getOrganisationQid(), 'create', $label);
        return $itemId;
      }
      catch (UsageException $ex) {
        // Error most likely thrown because item already exists with the exact
        // same label & description. Just return the existing ID in that case.
        $row[CsvErrorRowEnum::step2_error_code()->value] = 'modification-failed';
        $row[CsvErrorRowEnum::step2_error_message()->value] = $ex->getMessage();
        if (trim($row[CsvRowEnum::wikidata_id()->value])) {
          $id = trim($row[CsvRowEnum::wikidata_id()->value]);
          return new ItemId($id);
        }
      }
    }
    return NULL;
  }

  /**
   * Validates whether row data is complete enough for creating a new item.
   *
   * @param array $row
   *   The row data to validate.
   * @param bool $valid_description
   *   Whether generated description contains valid data.
   * @param \Drupal\general\Data\BatchGeneralData $generalData
   *   The general batch data.
   * @param \Drupal\taxonomy\TermInterface|null $occupation_term
   *   The occupation term.
   *
   * @return bool
   *   Whether data is complete enough for creating a new WikiData item.
   */
  protected function validDataForItemCreation(array $row, bool $valid_description, BatchGeneralData $generalData, ?TermInterface $occupation_term = NULL): bool {
    // Validate: row has label.
    if (empty($row[CsvRowEnum::creator()->value])) {
      return FALSE;
    }
    // Validate: row has description.
    if (!$valid_description) {
      return FALSE;
    }

    // Validate: instance of / type vervaardiger.
    if (empty($row[CsvRowEnum::instance_of()->value])) {
      return FALSE;
    }

    // Validate: occupation (for individu).
    $valid_occupation = $occupation_term && $occupation_term->get('field_qid')->getValue()[0]['value'] !== 'NONE';
    if (strtolower($row[CsvRowEnum::instance_of()->value]) == 'individu' && !$valid_occupation) {
      return FALSE;
    }

    // Validate: has works in collection.
    if (empty($generalData->getOrganisationQid())) {
      return FALSE;
    }

    // Validate: has proper reference to add to statements.
    if (!$this->validReferenceData($row, $generalData)) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Validate if this row has valid data to add references to statements.
   *
   * @param array $row
   *   The row data.
   * @param \Drupal\general\Data\BatchGeneralData $generalData
   *   The general batch data.
   *
   * @return bool
   *   Whether row has valid data to add references to statements.
   */
  protected function validReferenceData(array $row, BatchGeneralData $generalData): bool {
    $wbFactory = $this->getApi();
    $getter = $wbFactory->newRevisionGetter();

    if (filter_var($row[CsvRowEnum::identifier()->value], FILTER_VALIDATE_URL)) {
      return TRUE;
    }
    else {
      if ($dataset_qid = $generalData->getDatasetQid()) {
        if ($this->qidEntityConvert($dataset_qid, $getter)) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * Get label from Wikidata for a given item in a given language.
   *
   * @param string $qid
   *   The WikiData QID.
   * @param string $language
   *   The language code to get label in.
   *
   * @return string
   *   The label.
   */
  protected function getItemLabel(string $qid, string $language): string {
    $itemId = new ItemId($qid);
    $termLookup = $this->wbFactory->newTermLookup();
    if ($label = $termLookup->getLabel($itemId, $language)) {
      return $label;
    }
    return '';
  }

  /**
   * Checks for identical reference.
   *
   * @param \Wikibase\DataModel\Statement\Statement $existingStatement
   *   The existing statement.
   * @param array $row
   *   The row.
   * @param int|string $userQid
   *   The user qid.
   * @param string $value
   *   The value.
   *
   * @return bool
   *   Whether this has an identical reference.
   */
  protected function hasIdenticalReference(Statement $existingStatement, array $row, int|string $userQid, string $value): bool {
    $wd_endpoint = new WikiDataEndpoint();
    $countRefs = count($existingStatement->getReferences());
    $addRef = 0;
    foreach ($existingStatement->getReferences() as $reference) {
      $serialized = $reference->getSnaks()->serialize();
      $snaks = unserialize($serialized)['data'];
      $totalSnaksToCheck = 0;
      $addSnack = 0;
      foreach ($snaks as $snak) {
        $snakValue = $snak->getDataValue();
        if ($snakValue instanceof EntityIdValue) {
          $snakValue = 'Q' . $snak->getDataValue()->getEntityId()->getNumericId();
        }
        if ($snakValue instanceof MonolingualTextValue) {
          $snakValue = $snak->getDataValue()->getText();
        }
        if ($snakValue instanceof StringValue) {
          $snakValue = $snak->getDataValue()->getValue();
        }
        $propertyId = unserialize($snak->serialize())[0];
        if (($propertyId === $wd_endpoint->getProperty('reference url') && filter_var($row[CsvRowEnum::identifier()->value], FILTER_VALIDATE_URL))
          || $propertyId === $wd_endpoint->getProperty('stated in')
          || $propertyId === $wd_endpoint->getProperty('quotation')) {
          $totalSnaksToCheck++;
        }
        if (($propertyId === $wd_endpoint->getProperty('reference url') && $snakValue !== $row[CsvRowEnum::identifier()->value] && filter_var($row[CsvRowEnum::identifier()->value], FILTER_VALIDATE_URL))
          || ($propertyId === $wd_endpoint->getProperty('stated in') && $snakValue !== $userQid)
          || ($propertyId === $wd_endpoint->getProperty('quotation') && $snakValue !== $value)) {
          $addSnack++;
        }
      }
      if ($totalSnaksToCheck !== $addSnack) {
        $addRef++;
      }
    }
    if ($addRef === $countRefs) {
      return TRUE;
    }
    return FALSE;
  }

}
