<?php

namespace Drupal\general;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Wikidata update entity.
 *
 * @see \Drupal\general\Entity\WikidataUpdate.
 */
class WikidataUpdateAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {
    /** @var \Drupal\general\Entity\WikidataUpdateInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished wikidata update entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published wikidata update entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit wikidata update entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete wikidata update entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResultInterface {
    return AccessResult::allowedIfHasPermission($account, 'add wikidata update entities');
  }

}
