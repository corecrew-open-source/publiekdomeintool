<?php

/**
 * @file
 * Contains wikidata_update.page.inc.
 *
 * Page callback for Wikidata update entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Wikidata update templates.
 *
 * Default template: wikidata_update.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_wikidata_update(array &$variables): void {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
