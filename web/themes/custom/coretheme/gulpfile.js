(() => {

  'use strict';

  /**************** Gulp.js 4 configuration ****************/

  const

    // development or production
    devBuild  = ((process.env.NODE_ENV || 'development').trim().toLowerCase() === 'development'),

    // directory locations
    dir = {
      src         : '',
      build       : ''
    },

    // modules
    gulp          = require('gulp'),
    noop          = require('gulp-noop'),
    size          = require('gulp-size'),
    sass          = require('gulp-sass'),
    postcss       = require('gulp-postcss'),
    uglify        = require('gulp-uglify'),
    rename        = require('gulp-rename'),

    sourcemaps    = devBuild ? require('gulp-sourcemaps') : null;


  console.log('Gulp', devBuild ? 'development' : 'production', 'build');


  /**************** CSS task ****************/

  const cssConfig = {
    src         : dir.src + 'sass/**/*.scss',
    watch       : dir.src + 'sass/**/*.scss',
    build       : dir.build + 'css/',

    sassOpts: {
      sourceMap       : devBuild,
      outputStyle     : 'nested',
      imagePath       : '/images/',
      precision       : 3,
      errLogToConsole : true
    },

    postCSS: [
      require('postcss-assets')({
        loadPaths: ['images/'],
        basePath: dir.build
      }),
      require('autoprefixer')({
        cascade: false
      })
    ]
  };

  // remove unused selectors and minify production CSS
  if (!devBuild) {
    cssConfig.postCSS.push(
      require('cssnano')
    );
  }

  function css() {
    return gulp.src(cssConfig.src)
      .pipe(sourcemaps ? sourcemaps.init() : noop())
      .pipe(sass(cssConfig.sassOpts).on('error', sass.logError))
      .pipe(postcss(cssConfig.postCSS))
      .pipe(sourcemaps ? sourcemaps.write() : noop())
      .pipe(size({ showFiles:true }))
      .pipe(gulp.dest(cssConfig.build))
  }

  exports.css = gulp.series(css);

  /**************** JS task ****************/

  const jsConfig = {
    src         : dir.src + 'scripts/**/*.js',
    watch       : dir.src + 'scripts/**/*.js',
    build       : dir.build + 'js/',
  };

  function js() {
    return gulp.src(jsConfig.src)
      .pipe(uglify())
      .pipe(sourcemaps ? sourcemaps.write() : noop())
      .pipe(size({ showFiles:true }))
      .pipe(rename({
        suffix: '.min'
      }))
      .pipe(gulp.dest(jsConfig.build))
  }
  exports.js = gulp.series(js);


  /**************** watch task ****************/

  function watch(done) {

    // CSS changes
    gulp.watch(cssConfig.watch, css);

    // JS changes
    gulp.watch(jsConfig.watch, js);

    done();
  }

  /**************** default task ****************/

  exports.build = gulp.series(exports.css, exports.js);
  exports.default = gulp.series(exports.build, watch);
})();

