/**
 * @file
 */

(function ($, Drupal) {

/**
 * MatchHeight settings
 */
// Under certain conditions where the size of the page is dynamically changing,
// such as during resize or when adding new elements,
// browser bugs cause the page scroll position to change unexpectedly.
$.fn.matchHeight._maintainScroll = true;
// By default, the _update method is throttled to execute at a maximum rate
// of once every 80ms.
$.fn.matchHeight._throttle = 80;
// Execute matchheight.
$.fn.matchHeight._update();

var matchHeightElements = [
  '.equal-heights article',
  '.paragraph--type--logo-grid .logo',
  '.paragraph--group--two-column > .paragraph--group-inner > .paragraph--group-content > .field--name-field-content > .paragraph'
];

/**
 * Media Queries
 */
var mobileQuery = 'screen and (max-width:699px)';
var nonMobileQuery = 'screen and (min-width:700px)';

/**
 * Example
 */
Drupal.behaviors.example = {
  attach: function (context, settings) {
    // console.log('Drupal Behaviour');
  },
  mobile: function() {
    // console.log('Mobile');
  },
  mobileUnmatch : function() {
    // console.log('Unmatch Mobile');
  },
  nonMobile: function() {
    // console.log('Non Mobile');
  }
}

// Let the document know when the mouse is being used
document.body.addEventListener('mousedown', function() {
  document.body.classList.remove('using-keyboard');
  usingKeyboard = false;
});

// Re-enable focus styling when Tab is pressed
document.body.addEventListener('keydown', function(event) {
  if (event.keyCode === 9) {
    document.body.classList.add('using-keyboard');
    usingKeyboard = true;
  }
});


//equalHeights on images loaded
Drupal.behaviors.imagesLoaded = {
  attach: function (context, settings) {
    var loaded = 0;
    var noOfImages = $("img").length;
    $("img").on('load', function(){
      loaded++;
      if(noOfImages === loaded) {
        $(matchHeightElements.join(', ')).matchHeight({ byRow: true, property: 'min-height' });

        //logo grid
        $('.paragraph--type--logo-grid .logo img').each(function(){
          var thisHeight = $(this).height();
          var logoHeight = $(this).closest('.logo').height();
          if (thisHeight < logoHeight) {
            var padding = (logoHeight - thisHeight)/2;
            $(this).css('padding-top', padding);
          }
        });

        // vertical align column with single image
        $('.paragraph--group--two-column > .paragraph--group-inner > .paragraph--group-content > .field--name-field-content > .paragraph').each(function(){
          if ($(this).find('> .field--name-field-blocks > .paragraph').length == 1 && $(this).find('> .field--name-field-blocks > .paragraph--type--image').length == 1) {
            var padding = ($(this).height() - $(this).find('> .field--name-field-blocks > .paragraph').height())/2;
            if (padding > 0) {
              $(this).find('.field--name-field-blocks').css({
                'padding-top': padding,
                'text-align': 'center'
              });
            }
          }
        });

      }
    }).each(function(){
      if (this.complete) {
        $(this).trigger('load');
      }
    });
  },
};

/**
 * Toggle local tasks
 * show/hide local tasks
 */

Drupal.behaviors.corecrewActionLinksToggle = {
  attach: function (context, settings) {
    $(context).find('.region--header').once('corecrewActionLinksToggle').each(function () {
      var actionLinks = $('.block-local-tasks-block');
      var cog = $('.action-links-toggle');
      actionLinks.addClass('hidden');
      cog.on('click', function() {
          actionLinks.toggleClass('hidden');
      });
    });
  }
};

/**
 * Toggle mobile header
 * show/hide mobile header
 */
Drupal.behaviors.toggleMobileHeader = {
  attach: function (context, settings) {
    $('.region--header').once().on('click', '#mobile-menu-anchor', function(e){
      e.preventDefault();
      if ($(this).hasClass('is-active')) {
        $('.header-mobile').stop().slideUp();
      } else {
        $('.header-mobile').stop().slideDown();
      }
      $(this).toggleClass('is-active');
    });
  },
};

/**
 * errors table
 */
$(window).on('load', function(){
  $('table.errors').each(function(){
    var tableWidth = $(this).parent().width();
    var cellWidth = tableWidth / 10; // 10% error margin
    $(this).find('tr:first-child td').each(function(){
      cellWidth = cellWidth + $(this).width() + 17;
      console.log(tableWidth + ' ' + cellWidth);
      if (cellWidth > tableWidth) {
        var index = $(this).index() + 1;
        $('table.errors').find('td:nth-child(n + '+index+')').addClass('hidden');
        $('table.errors').find('th:nth-child(n + '+index+')').addClass('hidden');
        return false;
      }
    });
    if (cellWidth > tableWidth) {
      $('<button class="prev hidden">vorige</button><button class="next">volgende</button>').appendTo('table.errors');
      $('.errors-wrapper').on('click', '.next', function(){
        $('table.errors').find('.hidden + td:not(.hidden)').addClass('hidden');
        $('table.errors').find('.hidden + th:not(.hidden)').addClass('hidden');
        if ($('table.errors').find('.hidden + td:not(.hidden)').length == 0) {
          $('table.errors').find('td:nth-child(2)').addClass('hidden');
          $('table.errors').find('th:nth-child(2)').addClass('hidden');
        }
        $('table.errors').find('td:nth-child(n+2):not(.hidden) + .hidden').removeClass('hidden');
        $('table.errors').find('th:nth-child(n+2):not(.hidden) + .hidden').removeClass('hidden');
        if (!$('table.errors tr:first-child td:last-child').hasClass('hidden')) {
          $('.errors-wrapper .next').addClass('hidden');
        } else {
          $('.errors-wrapper .next').removeClass('hidden');
        }
        $('.errors-wrapper .prev').removeClass('hidden');
      });
      $('.errors-wrapper').on('click', '.prev', function(){
        $('table.errors').find('.hidden + td:not(.hidden)').prev().removeClass('hidden');
        $('table.errors').find('.hidden + th:not(.hidden)').prev().removeClass('hidden');
        $('table.errors').find('td:nth-child(n+2):not(.hidden) + .hidden').prev().addClass('hidden');
        $('table.errors').find('th:nth-child(n+2):not(.hidden) + .hidden').prev().addClass('hidden');
        if ($('table.errors').find('th:nth-child(n+2):not(.hidden) + .hidden').length == 0) {
          $('table.errors').find('td:last-child').addClass('hidden');
          $('table.errors').find('th:last-child').addClass('hidden');
        }
        if (!$('table.errors tr:first-child td:nth-child(2)').hasClass('hidden')) {
          $('.errors-wrapper .prev').addClass('hidden');
        } else {
          $('.errors-wrapper .prev').removeClass('hidden');
        }
        $('.errors-wrapper .next').removeClass('hidden');
      });
    }
  });
});

// move batch title
$('.header-desktop > h1').prependTo('.region--content-inner');

/**
 * Standard
 * paragraph group standard
 */
Drupal.behaviors.paragraphStandard = {
  attach: function (context, settings) {
    $('.paragraph--group--standard, .paragraph--group--two-column').on('click', '> .paragraph--group-inner > .paragraph--group-content > .field--name-field-content > .paragraph > .field--name-field-title > a', function(e){
      e.preventDefault();
    });
  },
};


/**
 * Accordeon
 * paragraph accordeon
 */
Drupal.behaviors.accordeon = {
  attach: function (context, settings) {
    $('.paragraph--group--accordeon .field--name-field-title').each(function(){
      var id = $(this).parent().find('.field--name-field-id').text().trim();
      if (id != '') {
        $(this).attr('id', id);
      }
      var hash = window.location.hash;
      if (hash == $(this).attr('id')) {
        $('body, html').animate({
          scrollTop: $this.offset().top
        }, 600);
      }
    });
    $('.paragraph--group--accordeon').once().on('click', '> .paragraph--group-inner > .paragraph--group-content > .field--name-field-content > .paragraph > .field--name-field-title > button', function(e){
      e.preventDefault();
      if (!$(this).parent().hasClass('is-active')) {
        $(this).parent().parent().parent().find('.field--name-field-blocks.is-active').slideUp();
        $(this).parent().parent().parent().find('.is-active').removeClass('is-active');
        $(this).parent().addClass('is-active');
        $(this).parent().parent().find('.field--name-field-blocks').addClass('is-active').slideDown();
      } else {
        $(this).parent().parent().parent().find('.field--name-field-blocks.is-active').slideUp();
        $(this).parent().parent().parent().find('.is-active').removeClass('is-active');
      }
    });
  },
};
$(document).ready(function(){
  var hash = window.location.hash;
  if (hash != '') {
    $(hash).find('button').click();
  }
});
/**
 * Tabs
 * paragraph tabs
 */
Drupal.behaviors.tabs = {
  attach: function (context, settings) {
    $('.paragraph--group--tabs').each(function(){
      if ($(this).find('.paragraph-tabs').length == 0) {
        $(this).find('> .paragraph--group-inner > .paragraph--group-content').wrapInner('<div class="paragraph-tabs"></div>');
        $(this).find('> .paragraph--group-inner > .paragraph--group-content').prepend('<div class="paragraph-tab-titles"></div>');
        $(this).find('> .paragraph--group-inner > .paragraph--group-content > .paragraph-tabs > .field--name-field-content > .paragraph--type--group-content').each(function(){
          var parent = $(this).closest('.paragraph--group-content').find('> .paragraph-tab-titles');
          $(this).find('> h2').appendTo(parent);
        });
        $(this).find('> .paragraph--group-inner > .paragraph--group-content > .paragraph-tab-titles h2:first-child').addClass('is-active');
        $(this).find('> .paragraph--group-inner > .paragraph--group-content > .paragraph-tabs > .field > .paragraph:first-child').addClass('is-active');
        $(this).on('click', '.paragraph-tab-titles button', function(e){
          e.preventDefault();
          if (!$(this).parent().hasClass('is-active')) {
            $(this).parent().parent().parent().find('.is-active').removeClass('is-active');
            $(this).parent().addClass('is-active');
            var index = $(this).parent().index();
            var increasedIndex = index + 1;
            $(this).parent().parent().next().find('> .field > .paragraph:nth-child('+increasedIndex+')').addClass('is-active');
          }
        });
      }
    });
  },
};


/**
 * Select
 * Wrap select form items
 */
Drupal.behaviors.select = {
  attach: function (context, settings) {
    $('select:not(.select2-hidden-accessible)').once().wrap('<div class="select"></div>');
    $('.select2-container').once().wrap('<div class="select"></div>');
  },
};

// cookie tooltips
Drupal.behaviors.cookieTooltips = {
  attach: function (context, settings) {
    $('.eu-cookie-compliance-category label').each(function(){
      if ($(this).find('.tooltip-icon').length == 0) {
        $(this).append('<span class="tooltip-icon">?</span>');
      }
    });
    $('.eu-cookie-compliance-category label').on('mouseenter', '.tooltip-icon', function(){
      var category = $(this).parent().prev().attr('value');
        setTimeout(function(){
          $('p.'+category).stop().fadeIn(400);
        }, 600);
    });
    $('.eu-cookie-compliance-category label').on('mouseleave', '.tooltip-icon', function(){
      var category = $(this).parent().prev().attr('value');
        setTimeout(function(){
          $('p.'+category).stop().fadeOut(400);
        }, 2000);
    });
  },
};

Drupal.behaviors.corecrewToggleCookiePreferences = {
  attach: function (context, settings) {
    setTimeout(function() {
      if ($('#eu-cookie-compliance-categories').length > 0) {
        var togglePreferences = Drupal.t('Voorkeuren aanpassen');
        var disagree = Drupal.t('Aanvaard geen cookies');
        console.log($('#togglePreferences').length);
        if ($('#togglePreferences').length == 0) {
          $('.eu-cookie-compliance-content .eu-cookie-compliance-message').append('<div id="togglePreferences"><a class="togglePreferences" href="#">'+togglePreferences+'</a></div>');
          $('.eu-cookie-compliance-banner #popup-buttons').append('<button class="disagree-button">'+disagree+'</button>');
        }
        $('.eu-cookie-compliance-content').on('click', '.togglePreferences', function(e){
          e.preventDefault();
          $(this).slideUp();
          $('#popup-buttons').slideUp();
          $('#eu-cookie-compliance-categories').slideDown();
          $('#sliding-popup').addClass('toggledPreferences');
        });
        $('.eu-cookie-compliance-content').on('click', '.disagree-button', function(e){
          e.preventDefault();
          $('.eu-cookie-compliance-category input:not([disabled])').prop('checked', '');
          $('.eu-cookie-compliance-content .eu-cookie-compliance-save-preferences-button').click();
        });
        // tooltips
        for (var i = 1; i <= $('#sliding-popup .tooltip').length; i++) {
          $('#sliding-popup .eu-cookie-compliance-category:nth-child('+i+')').append($('#sliding-popup .tooltip:nth-child(3)'))
        }
      }
    }, 500);
  }
};

/**
 * Initialize Enquire
 */
enquire.register(mobileQuery, {

  match : function() {
    Drupal.behaviors.example.mobile();
  },

  unmatch : function() {
    Drupal.behaviors.example.mobileUnmatch();
  },

  setup : function() {},
  deferSetup : true,

  destroy : function() {}

}).register(nonMobileQuery, {
  match : function() {
    Drupal.behaviors.example.nonMobile();
    $(matchHeightElements.join(', ')).matchHeight({ byRow: true, property: 'min-height' });
  },

  unmatch : function() {
    $(matchHeightElements.join(', ')).matchHeight({ property: 'min-height', remove: true });
  },

  setup : function() {},
  deferSetup : true,

  destroy : function() {}

});

})(jQuery, Drupal);
